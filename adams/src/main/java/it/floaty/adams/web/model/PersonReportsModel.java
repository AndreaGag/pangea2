package it.floaty.adams.web.model;

import java.util.Set;

import it.floaty.adams.domain.Person;
import it.floaty.adams.domain.Report;

public class PersonReportsModel implements Comparable<PersonReportsModel>{
	private Person person;
	private Set<Report> reports;
	
	public PersonReportsModel() {
		super();
	}
	public PersonReportsModel(Person person, Set<Report> reports) {
		this.person = person;
		this.reports = reports;
	}
	
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public Set<Report> getReports() {
		return reports;
	}
	public void setReports(Set<Report> reports) {
		this.reports = reports;
	}
	
	@Override
	public int compareTo(PersonReportsModel o) {
		return this.person.getName().compareTo(o.getPerson().getName());
	}

}
