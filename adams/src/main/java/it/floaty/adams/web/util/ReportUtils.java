package it.floaty.adams.web.util;

import java.util.HashSet;
import java.util.Set;

import it.floaty.adams.domain.Crash;
import it.floaty.adams.domain.Person;
import it.floaty.adams.domain.Road;
import it.floaty.adams.domain.User;
import it.floaty.adams.domain.Vehicle;

public class ReportUtils {

	public static Crash updateCrash(User user, Crash origin, Crash update) {
		switch (user.getType()) {
		case 0:			//Police
			//TODO CREARE CRASHID
			origin.setCrashCause(update.getCrashCause());
			origin.setCrashCity(update.getCrashCity());
			origin.setCrashType(update.getCrashType());
			origin.setDate(update.getDate());
			origin.setTime(update.getTime());
			origin.setLatitude(update.getLatitude());
			origin.setLongitude(update.getLatitude());
			origin.setLocation(update.getCrashCause());
			origin.setImpactType(update.getImpactType());
			origin.setWeatherConditions(update.getWeatherConditions());
			origin.setLocation(update.getLocation());
			origin.setLightConditions(update.getLightConditions());
			return origin;
		case 1:			//Hospital
			origin.setDateAdmission(update.getDateAdmission());
			origin.setTimeAdmission(update.getTimeAdmission());
			return origin;
		default:
			return origin;
		}
	}

	public static Set<Person> updatePersons(User user, Set<Person> origin, Set<Person> update) {
		Set<Person> toUpdatePersons = new HashSet<Person>();
		for(Person p:update) {
			Person toUpdatePerson = null;
			try {
				toUpdatePerson = origin.stream()
						.filter(x -> x.getName().equalsIgnoreCase(p.getName())).findFirst().get();
			} catch (Exception e) {}

			if(toUpdatePerson != null) {
				switch (user.getType()) {
				case 0:			//Police
					toUpdatePerson.setVehicleNumber(p.getVehicleNumber());
					toUpdatePerson.setRoadUserType(p.getRoadUserType());
					toUpdatePerson.setSeatingPosition(p.getSeatingPosition());
					toUpdatePerson.setInjuryServerity(p.getInjuryServerity());
					toUpdatePerson.setSafetyEquipment(p.getSafetyEquipment());
					toUpdatePerson.setPedestrianManoeuvre(p.getPedestrianManoeuvre());
					toUpdatePerson.setAlcoholUse(p.getAlcoholUse());
					toUpdatePerson.setDrugUse(p.getDrugUse());
					break;
				case 1:			//Hospital
					toUpdatePerson.setDateExit(p.getDateExit());
					toUpdatePerson.setTimeExit(p.getTimeExit());
					toUpdatePerson.setTypeOfInjury(p.getTypeOfInjury());
					toUpdatePerson.setFinalDiagnosis(p.getFinalDiagnosis());
					break;
				default:
					return origin;
				}
				toUpdatePersons.add(toUpdatePerson);
			}
			else {
				toUpdatePersons.add(p);
			}
		}
		return toUpdatePersons;
	}

	public static Set<Vehicle> updateVehicles(User user, Set<Vehicle> origin, Set<Vehicle> update){
		switch (user.getType()) {
		case 0:
			return update;
		default:
			return origin;
		}
	}
	
	public static Road updateRoad(User user, Road origin, Road update){
		switch (user.getType()) {
		case 0:
			return update;
		default:
			return origin;
		}
	}
}
