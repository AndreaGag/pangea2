package it.floaty.adams.web.model.request;

public class UserModel extends GeneralModel{
	
	private long id;
	private String username;
	private String password;
	private String name;
	private String designation;
	private int type;
	private boolean active;
	private long baseId;
	
	public UserModel(String username, String password, String name, String designation, int type, boolean active,
			long baseId, String token) {
		super(token);
		this.username = username;
		this.password = password;
		this.name = name;
		this.designation = designation;
		this.type = type;
		this.active = active;
		this.baseId = baseId;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public long getBaseId() {
		return baseId;
	}
	public void setBaseId(long baseId) {
		this.baseId = baseId;
	}
}
