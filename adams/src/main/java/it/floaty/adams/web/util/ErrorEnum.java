package it.floaty.adams.web.util;

public enum ErrorEnum {
	
	INVALID_TOKEN_ENUM,
	INVALID_CREDENTIALS,
	INVALID_PARAMETERS,
	INVALID_OPERATION
}
