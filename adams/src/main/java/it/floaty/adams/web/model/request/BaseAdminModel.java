package it.floaty.adams.web.model.request;

import javax.validation.constraints.NotNull;

import it.floaty.adams.domain.Admin;
import it.floaty.adams.domain.Base;

public class BaseAdminModel extends GeneralModel{

	@NotNull
	private Base base;
	@NotNull
	private Admin admin;
	
	public BaseAdminModel(Base base, Admin admin, String token) {
		super(token);
		this.base = base;
		this.admin = admin;
	}
	
	public Base getBase() {
		return base;
	}
	public void setBase(Base base) {
		this.base = base;
	}
	public Admin getAdmin() {
		return admin;
	}
	public void setAdmin(Admin admin) {
		this.admin = admin;
	}
}
