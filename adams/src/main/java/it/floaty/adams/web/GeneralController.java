package it.floaty.adams.web;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import it.floaty.adams.domain.Admin;
import it.floaty.adams.domain.Session;
import it.floaty.adams.domain.User;
import it.floaty.adams.persistence.AdminRepository;
import it.floaty.adams.persistence.SessionRepository;
import it.floaty.adams.persistence.UserRepository;

public abstract class GeneralController {

	@Autowired
	private SessionRepository sessionRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private AdminRepository adminRepository;
	
	public boolean validateSession(String token) {
		Session session = sessionRepository.findByToken(token);
		if(session != null && session.isValid(this)) return true;
		else return false;
	}
	
	public Session getSession(String token) {
		Session session = sessionRepository.findByToken(token);
		if(session != null && session.isValid(this)) return session;
		else return null;
	}
	
	public User getUserBySession(Session session) {
		Optional<User> user = userRepository.findById(session.getUserId());
		if(user.isPresent()) return user.get();
		else return null;
	}
	
	public Admin getAdminBySession(Session session) {
		Optional<Admin> admin = adminRepository.findById(session.getUserId());
		if(admin.isPresent()) return admin.get();
		else return null;
	}
	
	public void updateSession(Session session) {
		session.setDateTime(LocalDateTime.now());
		sessionRepository.save(session);
	}
}
