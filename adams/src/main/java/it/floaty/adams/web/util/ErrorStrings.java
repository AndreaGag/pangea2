package it.floaty.adams.web.util;

public class ErrorStrings {
	public final static String INVALID_TOKEN = "Invalid token, return to login";
	public final static String INVALID_CREDENTIALS = "Invalid username or password";
	public final static String INVALID_PARAMETERS = "Invalid parameters";
	public final static String INVALID_OPERATION = "Invalid operation";
	public final static String INVALID_USER = "User not exist";
}
