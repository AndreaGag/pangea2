package it.floaty.adams.web;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import it.floaty.adams.domain.Admin;
import it.floaty.adams.domain.Base;
import it.floaty.adams.domain.User;
import it.floaty.adams.persistence.AdminRepository;
import it.floaty.adams.persistence.BaseRepository;
import it.floaty.adams.persistence.SessionRepository;
import it.floaty.adams.persistence.UserRepository;
import it.floaty.adams.web.model.ResponseModel;
import it.floaty.adams.web.model.request.IdModel;
import it.floaty.adams.web.model.request.UserModel;
import it.floaty.adams.web.util.ErrorStrings;

@RestController
public class CollectorController extends GeneralController{

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private BaseRepository baseRepository;
	@Autowired
	private AdminRepository adminRepository;
	@Autowired
	private SessionRepository sessionRepository;

	//create
	@CrossOrigin(origins = {"*"})
	@PostMapping("/createCollector")
	public ResponseModel createCollector(@Valid @RequestBody UserModel model) {
		if(validateSession(model.getToken())) {
			long baseId = 0;
			Optional<Admin> admin = adminRepository.findById(sessionRepository.findByToken(model.getToken()).getAdminId());
			if(admin.isPresent()) baseId = admin.get().getBase().getId();
			Optional<Base> base = baseRepository.findById(baseId);
			if(base.isPresent()) {
				User user = new User(new User().getUserTypeFromBaseType(base.get().getType()), model.getUsername(), model.getPassword(), model.getName(), model.getDesignation(), true, base.get());
				user = this.userRepository.save(user);
				return new ResponseModel(user, 0, null);
			}
			else {
				return new ResponseModel(null, 1, ErrorStrings.INVALID_PARAMETERS);
			}
		}
		else {
			return new ResponseModel(null, 1, ErrorStrings.INVALID_TOKEN);
		}
	}

	//update
	@CrossOrigin(origins = {"*"})
	@PostMapping("/updateCollector")
	public ResponseModel updateCollector(@Valid @RequestBody UserModel model) {
		if(validateSession(model.getToken())) {
			Optional<User> user = this.userRepository.findById(model.getId());
			if(user.isPresent()) {
				User toUpUser = user.get();
				toUpUser.setUsername(model.getUsername());
				toUpUser.setPassword(model.getPassword());
				toUpUser.setName(model.getName());
				toUpUser.setDesignation(model.getDesignation());
				toUpUser = userRepository.save(toUpUser);
				return new ResponseModel(toUpUser, 0, null);
			}
			else {
				return new ResponseModel(null, 1, ErrorStrings.INVALID_PARAMETERS);
			}
		}
		else {
			return new ResponseModel(null, 1, ErrorStrings.INVALID_TOKEN);
		}
	}

	//delete
	@CrossOrigin(origins = {"*"})
	@PostMapping("/deleteCollector")
	public ResponseModel deleteCollector(@Valid @RequestBody IdModel model) {
		if(validateSession(model.getToken())) {
			userRepository.deleteById(model.getId());
			return new ResponseModel(null, 0, null);
		}
		else {
			return new ResponseModel(null, 1, ErrorStrings.INVALID_TOKEN);
		}
	}

	//getById
	@CrossOrigin(origins = {"*"})
	@PostMapping("/getCollectorsByBase")
	public ResponseModel getCollectorsByBase(@Valid @RequestBody IdModel model) {
		if(validateSession(model.getToken())) {
			Optional<Base> base = baseRepository.findById(model.getId());
			if(base.isPresent()) {
				List<User> users = userRepository.findByBase(base.get());
				return new ResponseModel(users, 0, null);
			}
			else {
				return new ResponseModel(null, 1, ErrorStrings.INVALID_PARAMETERS);
			}
		}
		else {
			return new ResponseModel(null, 1, ErrorStrings.INVALID_TOKEN);
		}
	}

	//activate-deactivate
	@CrossOrigin(origins = {"*"})
	@PostMapping("/updateCollectorState")
	public ResponseModel updateCollectorState(@Valid @RequestBody UserModel model) {
		if(validateSession(model.getToken())) {
			Optional<User> user = this.userRepository.findById(model.getId());
			if(user.isPresent()) {
				User toUpUser = user.get();
				toUpUser.setActive(!user.get().isActive());
				toUpUser = userRepository.save(toUpUser);
				return new ResponseModel(toUpUser, 0, null);
			}
			else {
				return new ResponseModel(null, 1, ErrorStrings.INVALID_PARAMETERS);
			}
		}
		else {
			return new ResponseModel(null, 1, ErrorStrings.INVALID_TOKEN);
		}
	}
}
