package it.floaty.adams.web.model.request;

import java.util.Set;

import javax.validation.constraints.NotNull;

import it.floaty.adams.domain.Crash;
import it.floaty.adams.domain.Person;
import it.floaty.adams.domain.Road;
import it.floaty.adams.domain.User;
import it.floaty.adams.domain.Vehicle;

public class CollectorModel extends GeneralModel{

	@NotNull
	private Crash crash;
	@NotNull
	private Set<Person> persons;
	private Set<Vehicle> vehicles;
	private Road road;
	
	private long reportId;
	private boolean completed;
	
	public CollectorModel(String token, @NotNull Crash crash, @NotNull Set<Person> persons, Set<Vehicle> vehicles,
			Road road, long reportId, boolean completed) {
		super(token);
		this.crash = crash;
		this.persons = persons;
		this.vehicles = vehicles;
		this.road = road;
		this.reportId = reportId;
		this.completed = completed;
	}
	public Crash getCrash() {
		return crash;
	}
	public void setCrash(Crash crash) {
		this.crash = crash;
	}
	public Set<Person> getPersons() {
		return persons;
	}
	public void setPersons(Set<Person> persons) {
		this.persons = persons;
	}
	public Set<Vehicle> getVehicles() {
		return vehicles;
	}
	public void setVehicles(Set<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}
	public Road getRoad() {
		return road;
	}
	public void setRoad(Road road) {
		this.road = road;
	}
	public long getReportId() {
		return reportId;
	}
	public void setReportId(long reportId) {
		this.reportId = reportId;
	}
	public boolean getCompleted() {
		return completed;
	}
}
