package it.floaty.adams.web.model;

import it.floaty.adams.domain.Admin;
import it.floaty.adams.domain.User;

public class AuthResponseModel {

	private String token;
	private Admin admin;
	private User user;
	
	public AuthResponseModel(String token, Admin admin, User user) {
		super();
		this.token = token;
		this.admin = admin;
		this.user = user;
	}
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Admin getAdmin() {
		return admin;
	}
	public void setAdmin(Admin admin) {
		this.admin = admin;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
}
