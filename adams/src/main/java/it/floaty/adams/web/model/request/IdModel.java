package it.floaty.adams.web.model.request;

import javax.validation.constraints.NotNull;

public class IdModel extends GeneralModel{
	
	@NotNull
	private long id;

	public IdModel(String token, @NotNull long id) {
		super(token);
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
