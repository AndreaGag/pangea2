package it.floaty.adams.web.model.request;

import java.util.Date;

public class AnalyticsModel extends GeneralModel{
	private Date startDate;
	private Date endDate;
	
	public AnalyticsModel(String token, Date startDate, Date endDate) {
		super(token);
		this.startDate = startDate;
		this.endDate = endDate;
	}
	
	public Date getStartDate() {
		return this.startDate;
	}
	
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public Date getEndDate() {
		return this.endDate;
	}
	
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}	
}
	