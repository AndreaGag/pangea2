package it.floaty.adams.web.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class ResponseModel {

	@JsonSerialize
	Object obj;
	
	int status;
	
	String error;
	
	public ResponseModel(Object object, int status, String error) {
		super();
		this.obj = object;
		this.status = status;
		this.error = error;
	}
	public Object getObj() {
		return obj;
	}
	public void setObj(Object object) {
		this.obj = object;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
}
