package it.floaty.adams.web.util;

import java.security.SecureRandom;
import java.time.LocalDate;

public class Utils {

	public static boolean isNullOrEmpty(String string) {
		if(string == null) return true;
		else if(string.trim().length() == 0) return true;
		else return false;
	}

	public static String createToken() {
		SecureRandom secureRandom = new SecureRandom();
		byte bytes[] = new byte[256];
		secureRandom.nextBytes(bytes);
		return bytes.toString();
	}

	public static String createCrashId(long maxCrashId, int userType) {
		return LocalDate.now().toString() + "_" + maxCrashId+1 + "_" + getSourceFromType(userType);
	}

	public static String getSourceFromType(int type) {
		switch (type) {
		case 3:
			return "PN";
		case 4:
			return "SS";
		case 5:
			return "GN";
		default:
			return "NOTYPE";
		}
	}
}
