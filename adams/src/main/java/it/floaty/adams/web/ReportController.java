package it.floaty.adams.web;

import java.time.YearMonth;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import it.floaty.adams.domain.Crash;
import it.floaty.adams.domain.MapReport;
import it.floaty.adams.domain.Person;
import it.floaty.adams.domain.Report;
import it.floaty.adams.domain.Session;
import it.floaty.adams.domain.User;
import it.floaty.adams.persistence.CrashRepository;
import it.floaty.adams.persistence.PersonRepository;
import it.floaty.adams.persistence.ReportRepository;
import it.floaty.adams.persistence.UserRepository;
import it.floaty.adams.web.model.PersonReportsModel;
import it.floaty.adams.web.model.ResponseModel;
import it.floaty.adams.web.model.request.AnalyticsModel;
import it.floaty.adams.web.model.request.CollectorModel;
import it.floaty.adams.web.model.request.GeneralModel;
import it.floaty.adams.web.model.request.IdModel;
import it.floaty.adams.web.model.request.PersonNameModel;
import it.floaty.adams.web.util.ErrorStrings;
import it.floaty.adams.web.util.ReportUtils;
import it.floaty.adams.web.util.Utils;

@RestController
public class ReportController extends GeneralController{

	@Autowired
	private ReportRepository reportRepository;
	@Autowired
	private PersonRepository personRepository;
	@Autowired
	private CrashRepository crashRepository;
	@Autowired
	private UserRepository userRepository;
	
	private int datas[][];

	public ReportController(ReportRepository reportRepository, PersonRepository personRepository, CrashRepository crashRepository, UserRepository userRepositery) {
		super();
		this.reportRepository = reportRepository;
		this.personRepository = personRepository;
		this.crashRepository = crashRepository;
	}
	
	@CrossOrigin(origins = {"*"})
	@PostMapping("/getReportById")
	public ResponseModel getReportbyId(@RequestBody IdModel model) {
		if(validateSession(model.getToken())) {
			Optional<Report> report = this.reportRepository.findById(model.getId());
			if(report.isPresent())
				return new ResponseModel(report.get(), 0, null);
			else
				return new ResponseModel(null, 1, ErrorStrings.INVALID_PARAMETERS);
		}
		else {
			return new ResponseModel(null, 1, ErrorStrings.INVALID_TOKEN);
		}
	}
	
	@CrossOrigin(origins = {"*"})
	@PostMapping("/getDraftReports")
	public ResponseModel getDraftReports(@RequestBody IdModel model){
		if(validateSession(model.getToken())) {
			long hospitalId = model.getId();
			Optional<User> hospital = this.userRepository.findById(hospitalId);
			
			if(hospital.isPresent()) {
				Set<Report> reports = this.reportRepository.findByCompleted(false);
				return new ResponseModel(reports, 0, null);
			}
			else
				return new ResponseModel(null, 1, ErrorStrings.INVALID_PARAMETERS);
		}
		else {
			return new ResponseModel(null, 1, ErrorStrings.INVALID_TOKEN);
		}
		
	}

	@CrossOrigin(origins = {"*"})
	@PostMapping("/getReportList")
	public ResponseModel getData(@RequestBody GeneralModel model){
		if(validateSession(model.getToken())) {
			List<Report> reports = this.reportRepository.findAll();
			return new ResponseModel(reports, 0, null);
		}
		else {
			return new ResponseModel(null, 1, ErrorStrings.INVALID_TOKEN);
		}

	}

	@CrossOrigin(origins = {"*"})
	@PostMapping("/getAllReports")
	public ResponseModel getAllReports(@RequestBody GeneralModel model){
		if(validateSession(model.getToken())) {
			List<Report> reports = this.reportRepository.findAll();
			List<MapReport> mapReports = new ArrayList<>(); 
			for(Report report: reports) {
				MapReport mapReport = new MapReport(report);
				mapReports.add(mapReport);
			}
			return new ResponseModel(mapReports, 0, null);
		}
		else {
			return new ResponseModel(null, 1, ErrorStrings.INVALID_TOKEN);
		}

	}
	
	@CrossOrigin(origins = {"*"})
	@PostMapping("/getUserReports")
	public ResponseModel getUserReports(@Valid @RequestBody PersonNameModel model) {
		if(validateSession(model.getToken())) {
			User user = getUserBySession(getSession(model.getToken()));
			Set<Person> persons = new HashSet<Person>();			
			List<PersonReportsModel> personsReportsModels = new ArrayList<PersonReportsModel>();
			Set<Report> reports = reportRepository.findByValid(false);

			String[] parts = model.getNameParts();
			for (String part:parts) {
				persons.addAll(personRepository.findByNameIgnoreCaseContaining(part));
			}
			for(Person p:persons) {
				Set<Report> temp = new HashSet<Report>();
				if(user.isPolice()) {
					temp = reports.stream().filter(
							x -> x.getPersons().contains(p) && 
							x.getUserHospital() != null).collect(Collectors.toSet());
				}
				else {
					temp = reports.stream().filter(
							x -> x.getPersons().contains(p) && 
							x.getUserPolice() != null).collect(Collectors.toSet());
				}
				personsReportsModels.add(new PersonReportsModel(p, temp));
			}
			Collections.sort(personsReportsModels);

			return new ResponseModel(personsReportsModels, 0, null);
		}
		else {
			return new ResponseModel(null, 1, ErrorStrings.INVALID_TOKEN);
		}
	}
	
	@CrossOrigin(origins = {"*"})
	@PostMapping("/getReport")
	public ResponseModel getReport(@Valid @RequestBody IdModel model){
		if(validateSession(model.getToken())) {
			Optional<Report> report = this.reportRepository.findById(model.getId());
			if(report.isPresent())
				return new ResponseModel(report.get(), 0, null);
			else
				return new ResponseModel(null, 1, ErrorStrings.INVALID_PARAMETERS);
		}
		else {
			return new ResponseModel(null, 1, ErrorStrings.INVALID_TOKEN);
		}
	}

	@CrossOrigin(origins = {"*"})
	@PostMapping("/insertReport")
	public ResponseModel insertReport(@Valid @RequestBody CollectorModel model) {
		Session session = getSession(model.getToken());
		if(session != null){
			if(session.getUserId() > 0) {
				User user = getUserBySession(session);
				Report report;
				if(model.getReportId() > 0) {
					Optional<Report> optReport = reportRepository.findById(model.getReportId());
					if(optReport.isPresent()) {
						report = optReport.get();
						report.setCompleted(model.getCompleted());
						report.setCrash(ReportUtils.updateCrash(user, report.getCrash(), model.getCrash()));
						report.setPersons(ReportUtils.updatePersons(user, report.getPersons(), model.getPersons()));
						report.setVehicles(ReportUtils.updateVehicles(user, report.getVehicles(), model.getVehicles()));
						report.setRoad(ReportUtils.updateRoad(user, report.getRoad(), model.getRoad()));
						if(user.isPolice())
							report.setUserPolice( user ); 
						else
							report.setUserHospital( user );
						report = reportRepository.save(report);
						return new ResponseModel(report, 0, null);
					}
					else {
						return new ResponseModel(null, 1, ErrorStrings.INVALID_PARAMETERS);
					}
				}
				else {
					report = new Report(false, false, model.getRoad(), model.getCrash(), model.getPersons(), model.getVehicles(), null, null);
					Crash crash = crashRepository.findFirstByOrderByIdDesc();
					long maxId = crash != null? crash.getId() : 1;
					report.getCrash().setCrashId(Utils.createCrashId(maxId, user.getType()));
					if(user.isPolice()) {
						report.setUserPolice(user);
						report = reportRepository.save(report);
						return new ResponseModel(report, 0, null);
					}
					else {
						report.setUserHospital(user);
						report = reportRepository.save(report);
						return new ResponseModel(report, 0, null);
					}
				}
			}
			else {
				return new ResponseModel(null, 1, ErrorStrings.INVALID_OPERATION);
			}
		}
		else {
			return new ResponseModel(null, 1, ErrorStrings.INVALID_TOKEN);
		}
	}
	

	@CrossOrigin(origins = {"*"})
	@PostMapping("/getReportsAnalytics")
	public ResponseModel getReportAnalytics(AnalyticsModel model ) {    
		if(validateSession(model.getToken())) {
			List<Report> reports = this.reportRepository.findAll();
			List<Report> results = new ArrayList<Report>();
			for(Report report: reports) {
				Crash crash = report.getCrash();
				Date crashDate = Date.from(crash.getDate().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
				if(crashDate.compareTo(model.getEndDate()) <= 0 && crashDate.compareTo(model.getStartDate()) >= 0) {
					results.add(report);
				}
			}
			
			//return new ResponseModel(results, 0, null);
			
			int [][] datas = this.getDataForIngiuryTypeGraph(model.getStartDate(), model.getEndDate(), results);
			return new ResponseModel(datas, 0, null);
		}
		else {
			return new ResponseModel(null, 1, ErrorStrings.INVALID_TOKEN);
		}
	}

	@SuppressWarnings("deprecation")
	private int[][] getDataForIngiuryTypeGraph(Date startDate, Date endDate, List<Report> results) {
		int slight = 0;
		int serious = 1;
		int fatal = 2;
		int slightCount = 0;
		int seriousCount = 0;
		int fatalCount = 0;
		datas = null;
		
		if (startDate.getYear() == endDate.getYear()) {
			
			if(startDate.getMonth() == endDate.getMonth()) {
				
				if(startDate.getDay() == endDate.getDay()) {
					
					for(Report report: results) {
						Set<Person> persons = report.getPersons();
						
						for(Person person: persons) {
							
							if(person.getInjuryServerity()==slight) {
								slightCount++;
							}
							
							if(person.getInjuryServerity()==serious) {
								seriousCount++;
							}
							
							if(person.getInjuryServerity()==fatal) {
								fatalCount++;
							}
						}
					}
					datas[0][0] = slightCount;
					datas[0][1] = seriousCount;
					datas[0][2] = fatalCount;
				}
				else {
					
					for(Report report: results) {
						
						Crash crash = report.getCrash();
						Set<Person> persons = report.getPersons();
						YearMonth ym = YearMonth.of(startDate.getYear(), startDate.getMonth());
						int daysInMonth = ym.lengthOfMonth();
						
						for( int i=1; i<=daysInMonth; i++ ) {
							
							for(Person person: persons) {
								
								if(i==crash.getDate().getDayOfMonth()) {
									
									if(person.getInjuryServerity()==slight) {
										slightCount++;
									}
									
									if(person.getInjuryServerity()==serious) {
										seriousCount++;
									}
									
									if(person.getInjuryServerity()==fatal) {
										fatalCount++;
									}
								}
							}
							datas[i][0] = slightCount;
							datas[i][1] = seriousCount;
							datas[i][2] = fatalCount;
						}	
					}
				}
			}
			else {
				
				for(Report report: results) {
					Crash crash = report.getCrash();
					Set<Person> persons = report.getPersons();
					
					for( int i=1; i<=12; i++ ) {
						
						for(Person person: persons) {
							
							if(i==crash.getDate().getMonthValue()) {
								
								if(person.getInjuryServerity()==slight) {
									slightCount++;
								}
								
								if(person.getInjuryServerity()==serious) {
									seriousCount++;
								}
								
								if(person.getInjuryServerity()==fatal) {
									fatalCount++;
								}
							}
						}
						datas[i][0] = slightCount;
						datas[i][1] = seriousCount;
						datas[i][2] = fatalCount;
					}
				}
			}
		}
		
		else {			
			for(Report report: results) {
				Crash crash = report.getCrash();
				Set<Person> persons = report.getPersons();
				
				for( int i=startDate.getYear(); i<=endDate.getYear(); i++ ) {
					
					for(Person person: persons) {
						
						if(i==crash.getDate().getYear()) {
							
							if(person.getInjuryServerity()==slight) {
								slightCount++;
							}
							
							if(person.getInjuryServerity()==serious) {
								seriousCount++;
							}
							
							if(person.getInjuryServerity()==fatal) {
								fatalCount++;
							}
						}
					}
					datas[i][0] = slightCount;
					datas[i][1] = seriousCount;
					datas[i][2] = fatalCount;
				}
			}
		}
		return datas;
	}
	
}
