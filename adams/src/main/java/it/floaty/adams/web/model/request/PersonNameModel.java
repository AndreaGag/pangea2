package it.floaty.adams.web.model.request;

public class PersonNameModel extends GeneralModel{

	private String name;
	

	public PersonNameModel(String token, String name) {
		super(token);
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String[] getNameParts(){
		return name.split(" ");
	}
}
