package it.floaty.adams.web;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import it.floaty.adams.domain.Admin;
import it.floaty.adams.domain.Session;
import it.floaty.adams.domain.User;
import it.floaty.adams.persistence.AdminRepository;
import it.floaty.adams.persistence.SessionRepository;
import it.floaty.adams.persistence.UserRepository;
import it.floaty.adams.web.model.AuthResponseModel;
import it.floaty.adams.web.model.ResponseModel;
import it.floaty.adams.web.model.request.LoginModel;
import it.floaty.adams.web.util.ErrorStrings;
import it.floaty.adams.web.util.Utils;

@RestController
public class AuthController extends GeneralController{

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private SessionRepository sessionRepository;
	@Autowired
	private AdminRepository adminRepository;
	
	@CrossOrigin
	@PostMapping("/auth")
	public ResponseModel auth(@RequestHeader(value="User-Agent") String userAgent, @RequestBody LoginModel model) {
		Session session = sessionRepository.findByToken(model.getToken());
		User user = userRepository.findByUsername(model.getUsername());
		Admin admin = adminRepository.findByUsername(model.getUsername());
		AuthResponseModel responseModel = new AuthResponseModel(null, admin, user);
		if(session == null && user == null && admin == null) return new ResponseModel(null, 1, ErrorStrings.INVALID_CREDENTIALS);
		if(user != null) {
			if(session != null) {
				if(session.isValid(this)) {
					responseModel.setToken(session.getToken()+user.getType());
					return new ResponseModel(responseModel, 0, "");
				}
				else if(model.getPassword() != null && user.getPassword().equals(model.getPassword())){
					sessionRepository.delete(session);
					session = createSession(user, userAgent);
					responseModel.setToken(session.getToken()+user.getType());
					return new ResponseModel(responseModel, 0, "");
				}
				else return new ResponseModel(null, 1, ErrorStrings.INVALID_CREDENTIALS);
			}
			else {
				if(user.getPassword().equals(model.getPassword())) {
					responseModel.setToken(createSession(user, userAgent).getToken()+user.getType());
					return new ResponseModel(responseModel, 0, "");
				}
				else
					return new ResponseModel(null, 1, ErrorStrings.INVALID_CREDENTIALS);
			}
		}
		else if(admin != null) {
			if(session != null) {
				if(session.isValid(this)) {
					responseModel.setToken(session.getToken()+admin.getBase().getType());
					return new ResponseModel(responseModel, 0, "");
				}
				else if(model.getPassword() != null && admin.getPassword().equals(model.getPassword())){
					sessionRepository.delete(session);
					session = createSession(admin, userAgent);
					responseModel.setToken(session.getToken()+admin.getBase().getType());
					return new ResponseModel(responseModel, 0, "");
				}
				else return new ResponseModel(null, 1, ErrorStrings.INVALID_CREDENTIALS);
			}
			else {
				if(admin.getPassword().equals(model.getPassword())) {
					responseModel.setToken(createSession(admin, userAgent).getToken()+admin.getBase().getType());
					return new ResponseModel(responseModel, 0, "");
				}
				else
					return new ResponseModel(null, 1, ErrorStrings.INVALID_CREDENTIALS);
			}
		}
		else {
			if(session != null) {
				if(session.isValid(this)) {
					responseModel.setToken(session.getToken());
					return new ResponseModel(responseModel, 0, "");
				}
				else return new ResponseModel(null, 1, ErrorStrings.INVALID_TOKEN);
			}
			else
				return new ResponseModel(null, 1, ErrorStrings.INVALID_CREDENTIALS);
		}
	}

	private Session createSession(User user, String userAgent) {
		Session session = new Session();
		session.setDateTime(LocalDateTime.now());
		session.setUserAgent(userAgent);
		session.setUserId(user.getId());
		session.setToken(Utils.createToken());
		session = sessionRepository.save(session);
		return session;
	}
	
	private Session createSession(Admin admin, String userAgent) {
		Session session = new Session();
		session.setDateTime(LocalDateTime.now());
		session.setUserAgent(userAgent);
		session.setAdminId(admin.getId());
		session.setToken(Utils.createToken());
		session = sessionRepository.save(session);
		return session;
	}
	
}
