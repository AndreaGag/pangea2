package it.floaty.adams.web.model.request;

public abstract class GeneralModel {

	protected String token;

	public GeneralModel(String token) {
		this.token = token;
	}
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
