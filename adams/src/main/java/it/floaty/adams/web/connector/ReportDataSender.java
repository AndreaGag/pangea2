package it.floaty.adams.web.connector;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import it.floaty.adams.domain.Report;
import it.floaty.adams.persistence.ReportRepository;

public class ReportDataSender {
	
	private final String ADAMS_KEY = "NDFNDS46-ADSFNC323_RESO7";

	@Autowired
	private ReportRepository reportRepository;
	
	public Set<Report> collectCompleteReports(){
		Set<Report> reports = reportRepository.findByValidAndSent(true, true);
		
		return reports;
	}	
	
}
