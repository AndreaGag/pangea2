package it.floaty.adams.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.floaty.adams.domain.Base;
import it.floaty.adams.domain.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	public User findByUsername(String username);
	
	public List<User> findByBase(Base base);
}
