package it.floaty.adams.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.floaty.adams.domain.Admin;

@Repository
public interface AdminRepository extends JpaRepository<Admin,Long> {

	public Admin findByUsername(String username);
}
