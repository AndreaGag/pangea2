package it.floaty.adams.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.floaty.adams.domain.Road;

@Repository
public interface RoadRepository extends JpaRepository<Road, Long> {

}
