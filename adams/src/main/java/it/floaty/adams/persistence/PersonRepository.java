package it.floaty.adams.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.floaty.adams.domain.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person,Long>{

	public List<Person> findByNameIgnoreCaseContaining(String name);
}
