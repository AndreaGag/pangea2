package it.floaty.adams.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import it.floaty.adams.domain.Admin;

@Component
public class DbAdams implements CommandLineRunner{

	@Autowired
	private CrashRepository crashRepository;
	@Autowired
	private PersonRepository personRepository;
	@Autowired
	private RoadRepository roadRepository;
	@Autowired
	private VehicleRepository vehicleRepository;
	@Autowired
	private AdminRepository adminRepository;
	@Autowired
	private BaseRepository baseRepository;
	@Autowired
	private ReportRepository reportRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private SessionRepository sessionRepository;
	
	public DbAdams(CrashRepository crashRepository, PersonRepository personRepository, RoadRepository roadRepository,
			VehicleRepository vehicleRepository, AdminRepository adminRepository, BaseRepository baseRepository,
			ReportRepository reportRepository, UserRepository userRepository, SessionRepository sessionRepository) {
		super();
		this.crashRepository = crashRepository;
		this.personRepository = personRepository;
		this.roadRepository = roadRepository;
		this.vehicleRepository = vehicleRepository;
		this.adminRepository = adminRepository;
		this.baseRepository = baseRepository;
		this.reportRepository = reportRepository;
		this.userRepository = userRepository;
		this.sessionRepository = sessionRepository;
	}



	@Override
	public void run(String... args) throws Exception {
		Admin admin = adminRepository.findByUsername("OSER") == null ? adminRepository.save(new Admin("OSER", "OSER", 7)) : null;
	}

}
