package it.floaty.adams.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.floaty.adams.domain.Base;

@Repository
public interface BaseRepository extends JpaRepository<Base, Long> {

}
