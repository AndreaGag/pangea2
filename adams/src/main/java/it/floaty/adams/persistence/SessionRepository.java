package it.floaty.adams.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.floaty.adams.domain.Session;

@Repository
public interface SessionRepository extends JpaRepository<Session, Long> {

	public Session findByToken(String token);
}
