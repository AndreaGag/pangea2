package it.floaty.adams.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;


@Entity
@Table(name= "admin", uniqueConstraints=@UniqueConstraint(columnNames="username"))
public class Admin {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@NotNull
	@Column(length = 50, unique = true)
	private String username;
	@NotNull
	private String password;
	@NotNull
	private int operationsType;
	
	@ManyToOne(fetch=FetchType.EAGER)
	private Base base;
	
	public Admin() {
		super();
	}

	public Admin(@NotNull String username, @NotNull String password, @NotNull int operationsType) {
		super();
		this.username = username;
		this.password = password;
		this.operationsType = operationsType;
	}

	public Admin(@NotNull String username, @NotNull String password, @NotNull int operationsType, Base base) {
		super();
		this.username = username;
		this.password = password;
		this.operationsType = operationsType;
		this.base = base;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public long getId() {
		return id;
	}
	public Base getBase() {
		if(base == null) {
			Base baset = new Base();
			baset.setType(7);
			return baset;
		}
		return base;
	}
	public void setBase(Base base) {
		this.base = base;
	}
	public int getOperationsType() {
		return operationsType;
	}
	public void setOperationsType(int operationsType) {
		this.operationsType = operationsType;
	}
}
