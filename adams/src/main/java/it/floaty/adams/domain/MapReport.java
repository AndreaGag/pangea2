package it.floaty.adams.domain;

import java.time.LocalDate;
import java.util.Set;

public class MapReport {
	private long id;
	private int injurySeverity;
	private float lat;
	private float lng;
	private int veh_num;
	private int per_num;
	private LocalDate date;
	private String location;
	
	public MapReport (Report report){
		this.setId(report.getId());
		this.injurySeverity = this.calInjurySeverity(report.getPersons());
		this.setLat(report.getCrash().getLatitude());
		this.setLng(report. getCrash().getLongitude());
		this.setVeh_num(report.getVehicles().size());
		this.setPer_num(report.getPersons().size());
		this.setDate(report.getCrash().getDate());
		this.setLocation(report.getCrash().getLocation());
	
		
	}
	
	private int calInjurySeverity(Set<Person> persons) {
		int returnInjury = 1;
		for(Person person : persons) {
			if(person.getInjuryServerity() >= returnInjury) {
				returnInjury = person.getInjuryServerity();
			}
		}
		return injurySeverity;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public float getLat() {
		return lat;
	}

	public void setLat(float lat) {
		this.lat = lat;
	}

	public int getVeh_num() {
		return veh_num;
	}

	public void setVeh_num(int veh_num) {
		this.veh_num = veh_num;
	}

	public float getLng() {
		return lng;
	}

	public void setLng(float lng) {
		this.lng = lng;
	}

	public int getPer_num() {
		return per_num;
	}

	public void setPer_num(int per_num) {
		this.per_num = per_num;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
}