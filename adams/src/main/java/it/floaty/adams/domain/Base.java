package it.floaty.adams.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Base {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@NotNull
	@Column(unique = true)
	private String name;
	
	@NotNull
	private int type = 0;

	@OneToMany(fetch=FetchType.LAZY)
	@Cascade(CascadeType.PERSIST)
	private Set<Admin> admins;
	
	@OneToMany(fetch=FetchType.LAZY)
	private Set<User> users;
	
	public Base() {
		super();
	}

	public Base(@NotNull String name,@NotNull int type, Admin admin, Set<User> users) {
		super();
		this.name = name;
		this.admins = new HashSet<Admin>();
		this.admins.add(admin);
		this.users = users;
		this.type = type;
	}
	
	public Base(@NotNull String name,@NotNull int type, Admin admin) {
		super();
		this.name = name;
		this.admins = new HashSet<Admin>();
		this.admins.add(admin);
		this.type = type;
	}


	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@JsonIgnore
	public Set<Admin> getAdmins() {
		return admins;
	}
	public void setAdmins(Set<Admin> admins) {
		this.admins = admins;
	}
	public Set<User> getUsers() {
		return users;
	}
	public void setUsers(Set<User> users) {
		this.users = users;
	}
	public long getId() {
		return id;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
}
