package it.floaty.adams.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Road {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private int roadwayType;
	private int roadFunctionalClass;
	private String speedLimit;
	private int roadObstacles;
	private int surfaceConditions;
	private int junction;
	private int trafficControl;
	
	public Road() {}
	
	public long getId() {
		return id;
	}
	public int getRoadwayType() {
		return roadwayType;
	}
	public void setRoadwayType(int roadwayType) {
		this.roadwayType = roadwayType;
	}
	public int getRoadFunctionalClass() {
		return roadFunctionalClass;
	}
	public void setRoadFunctionalClass(int roadFunctionalClass) {
		this.roadFunctionalClass = roadFunctionalClass;
	}
	public String getSpeedLimit() {
		return speedLimit;
	}
	public void setSpeedLimit(String speedLimit) {
		this.speedLimit = speedLimit;
	}
	public int isRoadObstacles() {
		return roadObstacles;
	}
	public void setRoadObstacles(int roadObstacles) {
		this.roadObstacles = roadObstacles;
	}
	public int getSurfaceConditions() {
		return surfaceConditions;
	}
	public void setSurfaceConditions(int surfaceConditions) {
		this.surfaceConditions = surfaceConditions;
	}
	public int getJunction() {
		return junction;
	}
	public void setJunction(int junction) {
		this.junction = junction;
	}
	public int getTrafficControl() {
		return trafficControl;
	}
	public void setTrafficControl(int trafficControl) {
		this.trafficControl = trafficControl;
	}
}
