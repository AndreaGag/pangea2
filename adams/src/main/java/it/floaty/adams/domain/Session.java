package it.floaty.adams.domain;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.floaty.adams.web.GeneralController;

@Entity
public class Session {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private long userId;
	private long adminId;
	@Column(unique = true)
	private String token;
	private String userAgent;
	private LocalDateTime dateTime;
	@Transient
	private boolean valid;

	public Session() {}


	public long getId() {
		return id;
	} 
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getUserAgent() {
		return userAgent;
	}
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	public LocalDateTime getDateTime() {
		return dateTime;
	}
	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}
	public long getAdminId() {
		return adminId;
	}
	public void setAdminId(long adminId) {
		this.adminId = adminId;
	}

	@JsonIgnore
	public boolean isValid(GeneralController controller) {
		checkValidity();
		if(valid) {
			controller.updateSession(this);
		}
		return valid;
	}	
	private void checkValidity() {
		if(((LocalDateTime.now().toEpochSecond(ZoneOffset.ofHours(2))) - this.dateTime.toEpochSecond(ZoneOffset.ofHours(2)) < 600) || (this.userAgent.contains("Android") || this.userAgent.contains("iPhone")))
			this.valid = true;
	}
}
