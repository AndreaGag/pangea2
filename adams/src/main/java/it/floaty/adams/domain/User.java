package it.floaty.adams.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(unique = true)
	private String username;
	private String password;
	private String name;
	private String designation;
	private int type;
	private boolean active;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@Cascade(CascadeType.SAVE_UPDATE)
	private Base base;
	
	public User() {
		
	}
	
	public User(int type, String username, String password, String name,
			 String designation, boolean active, Base base) {
		super();
		this.username = username;
		this.password = password;
		this.name = name;
		this.designation = designation;
		this.active = active;
		this.base = base;
		this.type = type;
	}
	public long getId() {
		return id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String officerName) {
		this.name = officerName;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Base getBase() {
		return base;
	}
	public void setBase(Base base) {
		this.base = base;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public boolean isPolice() {
		if(type == 3) return true;
		else return false;
	}
	public int getUserTypeFromBaseType(int baseType){
		switch (baseType) {
		case 0:
			return 3;
		case 1:
			return 4;
		case 2:
			return 5;
		default:
			return 999;
		}
	}
}
