package it.floaty.adams.domain;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
public class Report {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;

	private boolean valid;
	
	private boolean sent;
	
	private boolean completed;
	
	@OneToOne(fetch=FetchType.EAGER)
	@Cascade(CascadeType.PERSIST)
	private Road road;
	
	@OneToOne(fetch=FetchType.EAGER)
	@Cascade(CascadeType.PERSIST)
	private Crash crash;
	
	@OneToMany(fetch=FetchType.EAGER)
	@Cascade(CascadeType.PERSIST)
	private Set<Person> persons;
	
	@OneToMany(fetch=FetchType.EAGER)
	@Cascade(CascadeType.PERSIST)
	private Set<Vehicle> vehicles;
	
	@OneToOne(fetch=FetchType.EAGER)
	@Cascade(CascadeType.SAVE_UPDATE)
	private User userPolice;
	
	@OneToOne(fetch=FetchType.EAGER)
	@Cascade(CascadeType.SAVE_UPDATE)
	private User userHospital;
	
	public Report() {}
	
	public Report(boolean completed, boolean valid, Road road, Crash crash, Set<Person> persons, Set<Vehicle> vehicles,
			User userPolice, User userHospital) {
		super();
		this.completed = completed;
		this.valid = valid;
		this.road = road;
		this.crash = crash;
		this.persons = persons;
		this.vehicles = vehicles;
		this.userPolice = userPolice;
		this.userHospital = userHospital;
	}
	
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	public Road getRoad() {
		return road;
	}
	public void setRoad(Road road) {
		this.road = road;
	}
	public Crash getCrash() {
		return crash;
	}
	public void setCrash(Crash crash) {
		this.crash = crash;
	}
	public Set<Person> getPersons() {
		return persons;
	}
	public void setPersons(Set<Person> persons) {
		this.persons = persons;
	}
	public Set<Vehicle> getVehicles() {
		return vehicles;
	}
	public void setVehicles(Set<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}
	public User getUserPolice() {
		return userPolice;
	}
	public void setUserPolice(User userPolice) {
		this.userPolice = userPolice;
	}
	public User getUserHospital() {
		return userHospital;
	}
	public void setUserHospital(User userHospital) {
		this.userHospital = userHospital;
	}
	public long getId() {
		return id;
	}
	public boolean isSent() {
		return sent;
	}
	public void setSent(boolean sent) {
		this.sent = sent;
	}
	public boolean isCompleted() {
		return completed;
	}
	public void setCompleted(boolean completed) {
		this.completed = completed;
	}
}
