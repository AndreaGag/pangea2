package it.floaty.adams;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages= {"it.floaty.adams"})
@EntityScan(basePackages = {"it.floaty.adams"})
@ComponentScan(basePackages = { "it.floaty.adams"})
public class AdamsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdamsApplication.class, args);
	}
}
