package it.floaty.adamsoser.web.model.request;

import java.time.LocalTime;
import java.util.Set;

import javax.validation.constraints.NotNull;

public class FiltersModel {

	@NotNull
	private String dayOfWeek;
	@NotNull
	private int month;
	@NotNull
	private Set<Integer> year;
	@NotNull
	private Set<LocalTime> timeOfCrash;
	@NotNull
	private int crashType;
	@NotNull
	private int impactType;
	@NotNull
	private int weatherConditions;
	@NotNull
	private int lightConditions;
	@NotNull
	private int roadwayType;
	@NotNull
	private int roadFunctionalClass;
	@NotNull
	private int roadObstacles;
	@NotNull
	private int surfaceConditions;
	@NotNull
	private int junction;
	@NotNull
	private int trafficControl;
	@NotNull
	private Set<Integer> speedLimit;
	@NotNull
	private int vehicleType;
	@NotNull
	private Set<Integer> vehicleModelYear;
	@NotNull
	private int vehicleSpecialFunction;
	@NotNull
	private int vehicleManoeuvre;
	@NotNull
	private Set<Integer> age;
	@NotNull
	private int sex;
	@NotNull
	private int roadUserType;
	@NotNull
	private int seatingPosition;
	@NotNull
	private int injurySeverity;
	@NotNull
	private int safetyEquipment;
	@NotNull
	private int pedestrianManoeuvre;
	@NotNull
	private int alcoholUse;
	@NotNull
	private int drugUse;
	@NotNull
	private Set<LocalTime> timeOfAdmission;
	@NotNull
	private String dayOfAdmission;
	@NotNull
	private int monthOfAdmission;
	@NotNull
	private Set<LocalTime> timeOfExit;
	@NotNull
	private String dayOfExit;
	@NotNull
	private int monthOfExit;
	@NotNull
	private int typeOfInjury;
	
	
	public FiltersModel(@NotNull String dayOfWeek, @NotNull int month, @NotNull Set<Integer> year,
			@NotNull Set<LocalTime> timeOfCrash, @NotNull int crashType, @NotNull int impactType,
			@NotNull int weatherConditions, @NotNull int lightConditions, @NotNull int roadwayType,
			@NotNull int roadFunctionalClass, @NotNull int roadObstacles, @NotNull int surfaceConditions,
			@NotNull int junction, @NotNull int trafficControl, @NotNull Set<Integer> speedLimit,
			@NotNull int vehicleType, @NotNull Set<Integer> vehicleModelYear, @NotNull int vehicleSpecialFunction,
			@NotNull int vehicleManoeuvre, @NotNull Set<Integer> age, @NotNull int sex, @NotNull int roadUserType,
			@NotNull int seatingPosition, @NotNull int injurySeverity, @NotNull int safetyEquipment,
			@NotNull int pedestrianManoeuvre, @NotNull int alcoholUse, @NotNull int drugUse,
			@NotNull Set<LocalTime> timeOfAdmission, @NotNull String dayOfAdmission, @NotNull int monthOfAdmission,
			@NotNull Set<LocalTime> timeOfExit, @NotNull String dayOfExit, @NotNull int monthOfExit,
			@NotNull int typeOfInjury) {
		super();
		this.dayOfWeek = dayOfWeek;
		this.month = month;
		this.year = year;
		this.timeOfCrash = timeOfCrash;
		this.crashType = crashType;
		this.impactType = impactType;
		this.weatherConditions = weatherConditions;
		this.lightConditions = lightConditions;
		this.roadwayType = roadwayType;
		this.roadFunctionalClass = roadFunctionalClass;
		this.roadObstacles = roadObstacles;
		this.surfaceConditions = surfaceConditions;
		this.junction = junction;
		this.trafficControl = trafficControl;
		this.speedLimit = speedLimit;
		this.vehicleType = vehicleType;
		this.vehicleModelYear = vehicleModelYear;
		this.vehicleSpecialFunction = vehicleSpecialFunction;
		this.vehicleManoeuvre = vehicleManoeuvre;
		this.age = age;
		this.sex = sex;
		this.roadUserType = roadUserType;
		this.seatingPosition = seatingPosition;
		this.injurySeverity = injurySeverity;
		this.safetyEquipment = safetyEquipment;
		this.pedestrianManoeuvre = pedestrianManoeuvre;
		this.alcoholUse = alcoholUse;
		this.drugUse = drugUse;
		this.timeOfAdmission = timeOfAdmission;
		this.dayOfAdmission = dayOfAdmission;
		this.monthOfAdmission = monthOfAdmission;
		this.timeOfExit = timeOfExit;
		this.dayOfExit = dayOfExit;
		this.monthOfExit = monthOfExit;
		this.typeOfInjury = typeOfInjury;
	}
}
