package it.floaty.web_police;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;

import it.floaty.domain_police.Crash;
import it.floaty.domain_police.Session;
import it.floaty.domain_police.User;
import it.floaty.models_police.CollectorModel;
import it.floaty.models_police.IdModel;
import it.floaty.models_police.LoginModel;
import it.floaty.models_police.ResponseModel;
import it.floaty.persistence_police.CrashRepository;
import it.floaty.persistence_police.SessionRepository;
import it.floaty.persistence_police.UserRepository;
import it.floaty.utils_police.Utils;

/**
 * @author fmelito
 *
 */

@RestController
public class PoliceController {

	@Autowired
	private CrashRepository crashRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private SessionRepository sessionRepository;

	public PoliceController(CrashRepository crashRepository, UserRepository userRepository, SessionRepository sessionRepository) {
		super();
		this.crashRepository = crashRepository;
		this.userRepository = userRepository;
		this.sessionRepository = sessionRepository;
	}
	@CrossOrigin(origins = {"*"})
	@GetMapping("/getData")
	public List<Crash> getData(){
		List<Crash> crashes = this.crashRepository.findAll();
		return crashes;
	}

	@CrossOrigin(origins = {"*"})
	@PostMapping("/getReport")
	public Crash getReport(@RequestBody IdModel model){
		Optional<Crash> crash = this.crashRepository.findById(model.getId());
		if(crash.isPresent())
			return crash.get();
		else
			return null;
	}

	@CrossOrigin(origins = {"*"})
	@PostMapping("/postData")
	public void postData(@Valid @RequestBody CollectorModel model) {
		Crash tempCrash = model.getCrash();
		tempCrash.setPersons(model.getPersons());
		tempCrash.setRoad(model.getRoad());
		tempCrash.setVehicles(model.getVehicles());
		tempCrash.setUser(model.getUser());

		this.crashRepository.save(tempCrash);
	}

	@CrossOrigin
	@PostMapping("/auth")
	public ResponseModel auth(@RequestHeader(value="User-Agent") String userAgent, @RequestBody LoginModel model) {
		Session session = sessionRepository.findByToken(model.getToken());
		User user = userRepository.findByUsername(model.getUsername());

		if(user != null) {
			if(session != null) {
				if(session.isValid()) 
					return new ResponseModel(session.getToken(), 0, "");
				else if(model.getPassword() != null && user.getPassword().equals(model.getPassword())){
					sessionRepository.delete(session);
					session = createSession(user, userAgent);
					return new ResponseModel(session.getToken(), 0, "");
				}
				else return new ResponseModel(null, 1, "Username o password errate");
			}
			else {
				if(user.getPassword().equals(model.getPassword()))
					return new ResponseModel(createSession(user, userAgent), 0, "");
				else
					return new ResponseModel(null, 1, "Username o password errate");
			}
		}
		else {
			if(session != null && session.isValid())
				return new ResponseModel(session.getToken(), 0, "");
			else
				return new ResponseModel(null, 1, "Token non valido, effettuare il log-in");
		}
	}

	private Session createSession(User user, String userAgent) {
		Session session = new Session();
		session.setDateTime(LocalDateTime.now());
		session.setUserAgent(userAgent);
		session.setUserId(user.getId());
		session.setToken(Utils.createToken());
		session = sessionRepository.save(session);
		return session;
	}
}
