package it.floaty.adams.web;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import it.floaty.adams.domain.Crash;
import it.floaty.adams.domain.Person;
import it.floaty.adams.domain.Report;
import it.floaty.adams.domain.Session;
import it.floaty.adams.domain.User;
import it.floaty.adams.persistence.CrashRepository;
import it.floaty.adams.persistence.PersonRepository;
import it.floaty.adams.persistence.ReportRepository;
import it.floaty.adams.web.model.PersonReportsModel;
import it.floaty.adams.web.model.ResponseModel;
import it.floaty.adams.web.model.request.CollectorModel;
import it.floaty.adams.web.model.request.GeneralModel;
import it.floaty.adams.web.model.request.IdModel;
import it.floaty.adams.web.model.request.PersonNameModel;
import it.floaty.adams.web.util.ErrorStrings;
import it.floaty.adams.web.util.ReportUtils;
import it.floaty.adams.web.util.Utils;

@RestController
public class ReportController extends GeneralController{

	@Autowired
	private ReportRepository reportRepository;
	@Autowired
	private PersonRepository personRepository;
	@Autowired
	private CrashRepository crashRepository;
	
	public ReportController(ReportRepository reportRepository, PersonRepository personRepository, CrashRepository crashRepository) {
		super();
		this.reportRepository = reportRepository;
		this.personRepository = personRepository;
		this.crashRepository = crashRepository;
	}
	
	
	@CrossOrigin(origins = {"*"})
	@PostMapping("/getReportList")
	public ResponseModel getData(@RequestBody GeneralModel model){
		if(validateSession(model.getToken())) {
			List<Report> reports = this.reportRepository.findAll();
			return new ResponseModel(reports, 0, null);
		}
		else {
			return new ResponseModel(null, 1, ErrorStrings.INVALID_TOKEN);
		}

	}

	@CrossOrigin(origins = {"*"})
	@PostMapping("/getReport")
	public ResponseModel getReport(@Valid @RequestBody IdModel model){
		if(validateSession(model.getToken())) {
			Optional<Report> report = this.reportRepository.findById(model.getId());
			if(report.isPresent())
				return new ResponseModel(report.get(), 0, null);
			else
				return new ResponseModel(null, 1, ErrorStrings.INVALID_PARAMETERS);
		}
		else {
			return new ResponseModel(null, 1, ErrorStrings.INVALID_TOKEN);
		}
	}

	@CrossOrigin(origins = {"*"})
	@PostMapping("/getUserReports")
	public ResponseModel getUserReports(@Valid @RequestBody PersonNameModel model) {
		if(validateSession(model.getToken())) {
			Set<Person> persons = new HashSet<Person>();			
			List<PersonReportsModel> personsReportsModels = new ArrayList<PersonReportsModel>();
			Set<Report> reports = reportRepository.findByValid(false);
			
			String[] parts = model.getNameParts();
			for (String part:parts) {
				persons.addAll(personRepository.findByNameIgnoreCaseContaining(part));
			}
			for(Person p:persons) {
				Set<Report> temp = new HashSet<Report>();
				temp = reports.stream()
						.filter(x -> x.isValid() == false && x.getPersons().contains(p)).collect(Collectors.toSet());
				personsReportsModels.add(new PersonReportsModel(p, temp));
			}
			Collections.sort(personsReportsModels);
			
			return new ResponseModel(personsReportsModels, 0, null);
		}
		else {
			return new ResponseModel(null, 1, ErrorStrings.INVALID_TOKEN);
		}
	}
	
	@CrossOrigin(origins = {"*"})
	@PostMapping("/insertReport")
	public ResponseModel insertReport(@Valid @RequestBody CollectorModel model) {
		Session session = getSession(model.getToken());
		if(session != null){
			if(session.getUserId() > 0) {
				User user = getUserBySession(session);
				Report report;
				if(model.getReportId() > 0) {
					Optional<Report> optReport = reportRepository.findById(model.getReportId());
					if(optReport.isPresent()) {
						report = optReport.get();
						report.setCrash(ReportUtils.updateCrash(user, report.getCrash(), model.getCrash()));
						report.setPersons(ReportUtils.updatePersons(user, report.getPersons(), model.getPersons()));
						report.setVehicles(ReportUtils.updateVehicles(user, report.getVehicles(), model.getVehicles()));
						report.setRoad(ReportUtils.updateRoad(user, report.getRoad(), model.getRoad()));
						if(user.isPolice())
							report.setUserPolice( user); 
						else
							report.setUserHospital( user);
						report = reportRepository.save(report);
						return new ResponseModel(report, 0, null);
					}
					else {
						return new ResponseModel(null, 1, ErrorStrings.INVALID_PARAMETERS);
					}
				}
				else {
					report = new Report(false, model.getRoad(), model.getCrash(), model.getPersons(), model.getVehicles(), null, null);
					Crash crash = crashRepository.findFirstByOrderByIdDesc();
					long maxId = crash != null? crash.getId() : 1;
					report.getCrash().setCrashId(Utils.createCrashId(maxId, user.getType()));
					if(user.isPolice()) {
						report.setUserPolice(user);
						report = reportRepository.save(report);
						return new ResponseModel(report, 0, null);
					}
					else {
						report.setUserHospital(user);
						report = reportRepository.save(report);
						return new ResponseModel(report, 0, null);
					}
				}
			}
			else {
				return new ResponseModel(null, 1, ErrorStrings.INVALID_OPERATION);
			}
		}
		else {
			return new ResponseModel(null, 1, ErrorStrings.INVALID_TOKEN);
		}
	}
}
