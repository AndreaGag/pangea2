package it.floaty.adams.web;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import it.floaty.adams.domain.Admin;
import it.floaty.adams.domain.Base;
import it.floaty.adams.persistence.AdminRepository;
import it.floaty.adams.persistence.BaseRepository;
import it.floaty.adams.web.model.ResponseModel;
import it.floaty.adams.web.model.request.AdminModel;
import it.floaty.adams.web.model.request.BaseAdminModel;
import it.floaty.adams.web.model.request.GeneralModel;
import it.floaty.adams.web.model.request.IdModel;
import it.floaty.adams.web.model.request.UserModel;
import it.floaty.adams.web.util.ErrorStrings;

@RestController
public class AdminController extends GeneralController{
	
	@Autowired
	private AdminRepository adminRepository;
	@Autowired
	private BaseRepository baseRepository;
	
	
	public AdminController(AdminRepository adminRepository, BaseRepository baseRepository) {
		super();
		this.adminRepository = adminRepository;
		this.baseRepository = baseRepository;
	}
	
	@CrossOrigin(origins = {"*"})
	@PostMapping("/createBaseAndAdmin")
	public ResponseModel createBaseAndAdmin(@Valid @RequestBody BaseAdminModel model) {
		if(validateSession(model.getToken())) {
			Base base = new Base(model.getBase().getName(), model.getBase().getType(), model.getAdmin());
			base.setType(model.getBase().getType());

			base = this.baseRepository.save(base);
			for(Admin a:base.getAdmins()){
				a.setBase(base);
			}
			this.baseRepository.save(base);
			
			return new ResponseModel(base, 0, null);
		}
		else {
			return new ResponseModel(null, 0, ErrorStrings.INVALID_TOKEN);
		}
	}
	
	@CrossOrigin(origins = {"*"})
	@PostMapping("/createAdmin")
	public ResponseModel createAdmin(@Valid @RequestBody AdminModel model) {
		if(validateSession(model.getToken())) {
			Optional<Base> base = baseRepository.findById(model.getBaseId());
			if(base.isPresent()) {
				Admin admin = new Admin(model.getUsername(), model.getPassword(), 0, base.get());
				admin = this.adminRepository.save(admin);
				return new ResponseModel(admin, 0, null);
			}
			else {
				return new ResponseModel(null, 1, ErrorStrings.INVALID_PARAMETERS);
			}
		}
		else {
			return new ResponseModel(null, 1, ErrorStrings.INVALID_TOKEN);
		}
	}
	
	@CrossOrigin(origins = {"*"})
	@PostMapping("/updateAdmin")
	public ResponseModel updateAdmin(@Valid @RequestBody AdminModel model) {
		if(validateSession(model.getToken())) {
			Optional<Admin> admin = this.adminRepository.findById(model.getId());
			if(admin.isPresent()) {
				Admin toUpAdmin = admin.get();
				toUpAdmin.setUsername(model.getUsername());
				toUpAdmin.setPassword(model.getPassword());
				toUpAdmin = adminRepository.save(toUpAdmin);
				return new ResponseModel(toUpAdmin, 0, null);
			}
			else {
				return new ResponseModel(null, 1, ErrorStrings.INVALID_PARAMETERS);
			}
		}
		else {
			return new ResponseModel(null, 1, ErrorStrings.INVALID_TOKEN);
		}
	}
	
	@CrossOrigin(origins = {"*"})
	@PostMapping("/deleteAdmin")
	public ResponseModel deleteAdmin(@Valid @RequestBody IdModel model) {
		if(validateSession(model.getToken())) {
			adminRepository.deleteById(model.getId());
			return new ResponseModel(null, 0, null);
		}
		else {
			return new ResponseModel(null, 1, ErrorStrings.INVALID_TOKEN);
		}
	}
	
	@CrossOrigin(origins = {"*"})
	@PostMapping("/getAdminList")
	public ResponseModel getAdminList(@Valid @RequestBody UserModel model) {
		if(validateSession(model.getToken())) {
			List<Admin> admins = adminRepository.findAll();
			return new ResponseModel(admins, 0, null);
		}
		else {
			return new ResponseModel(null, 1, ErrorStrings.INVALID_TOKEN);
		}
	}
}
