package it.floaty.adamsoser.domain;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Crash {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@NotNull
	private String crashId;
	@NotNull
	private LocalDate date;
	@NotNull
	private LocalTime time;
	@NotNull
	private String location;

	private String crashCause;
	private int impactType;
	private float latitude;
	private float longitude;
	private int crashType;
	private String crashCity;
	private int lightConditions;
	private int weatherConditions;
	private LocalDate dateAdmission;
	private LocalTime timeAdmission;
	
	public Crash() {
		super();
	}

	public Crash(String crashId, LocalDate date, LocalTime time, String location, String crashCause, int impactType,
			float latitude, float longitude, int crashType, String crashCity, int lightConditions,
			int weatherConditions) {
		super();
		this.crashId = crashId;
		this.date = date;
		this.time = time;
		this.location = location;
		this.crashCause = crashCause;
		this.impactType = impactType;
		this.latitude = latitude;
		this.longitude = longitude;
		this.crashType = crashType;
		this.crashCity = crashCity;
		this.lightConditions = lightConditions;
		this.weatherConditions = weatherConditions;
	}
	
	public Crash(long id, String crashId, LocalDate date, LocalTime time, String location, LocalDate dateAdmission,
			LocalTime timeAdmission) {
		super();
		this.id = id;
		this.crashId = crashId;
		this.date = date;
		this.time = time;
		this.location = location;
		this.dateAdmission = dateAdmission;
		this.timeAdmission = timeAdmission;
	}

	public long getId() {
		return id;
	}
	public String getCrashCause() {
		return crashCause;
	}
	public void setCrashCause(String crashCause) {
		this.crashCause = crashCause;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public LocalTime getTime() {
		return time;
	}
	public void setTime(LocalTime time) {
		this.time = time;
	}
	public String getCrashId() {
		return crashId;
	}
	public void setCrashId(String crashId) {
		this.crashId = crashId;
	}
	public int getImpactType() {
		return impactType;
	}
	public void setImpactType(int impactType) {
		this.impactType = impactType;
	}
	public float getLatitude() {
		return latitude;
	}
	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}
	public float getLongitude() {
		return longitude;
	}
	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}
	public int getCrashType() {
		return crashType;
	}
	public void setCrashType(int crashType) {
		this.crashType = crashType;
	}
	public String getCrashCity() {
		return crashCity;
	}
	public void setCrashCity(String crashCity) {
		this.crashCity = crashCity;
	}
	public int getLightConditions() {
		return lightConditions;
	}
	public void setLightConditions(int lightConditions) {
		this.lightConditions = lightConditions;
	}
	public int getWeatherConditions() {
		return weatherConditions;
	}
	public void setWeatherConditions(int weatherConditions) {
		this.weatherConditions = weatherConditions;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public LocalDate getDateAdmission() {
		return dateAdmission;
	}
	public void setDateAdmission(LocalDate dateAdmission) {
		this.dateAdmission = dateAdmission;
	}
	public LocalTime getTimeAdmission() {
		return timeAdmission;
	}
	public void setTimeAdmission(LocalTime timeAdmission) {
		this.timeAdmission = timeAdmission;
	}
}
