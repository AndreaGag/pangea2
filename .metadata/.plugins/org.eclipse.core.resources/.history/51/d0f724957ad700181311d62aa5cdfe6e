package it.floaty.adamsoser.web.model.request;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Set;

import javax.validation.constraints.NotNull;

public class FiltersModel {

	private DayOfWeek dayOfWeek;
	private int month;
	private Set<Integer> year;
	private Set<LocalTime> timeOfCrash;
	private int crashType;
	private int impactType;
	private int weatherConditions;
	private int lightConditions;
	private int roadwayType;
	private int roadFunctionalClass;
	private int roadObstacles;
	private int surfaceConditions;
	private int junction;
	private int trafficControl;
	private Set<Integer> speedLimit;
	private int vehicleType;
	private Set<Integer> vehicleModelYear;
	private int vehicleSpecialFunction;
	private int vehicleManoeuvre;
	private Set<Integer> age;
	private int sex;
	private int roadUserType;
	private int seatingPosition;
	private int injurySeverity;
	private int safetyEquipment;
	private int pedestrianManoeuvre;
	private int alcoholUse;
	private int drugUse;
	private Set<LocalTime> timeOfAdmission;
	private String dayOfAdmission;
	private int monthOfAdmission;
	private Set<LocalTime> timeOfExit;
	private String dayOfExit;
	private int monthOfExit;
	private int typeOfInjury;
	
	public FiltersModel() {
		super();
	}
	
	public FiltersModel(DayOfWeek dayOfWeek, int month,
			Set<Integer> year, Set<LocalTime> timeOfCrash, int crashType, int impactType, int weatherConditions,
			int lightConditions, int roadwayType, int roadFunctionalClass, int roadObstacles, int surfaceConditions,
			int junction, int trafficControl, Set<Integer> speedLimit, int vehicleType, Set<Integer> vehicleModelYear,
			int vehicleSpecialFunction, int vehicleManoeuvre, Set<Integer> age, int sex, int roadUserType,
			int seatingPosition, int injurySeverity, int safetyEquipment, int pedestrianManoeuvre, int alcoholUse,
			int drugUse, Set<LocalTime> timeOfAdmission, String dayOfAdmission, int monthOfAdmission,
			Set<LocalTime> timeOfExit, String dayOfExit, int monthOfExit, int typeOfInjury) {
		super();
		this.start = start;
		this.end = end;
		this.dayOfWeek = dayOfWeek;
		this.month = month;
		this.year = year;
		this.timeOfCrash = timeOfCrash;
		this.crashType = crashType;
		this.impactType = impactType;
		this.weatherConditions = weatherConditions;
		this.lightConditions = lightConditions;
		this.roadwayType = roadwayType;
		this.roadFunctionalClass = roadFunctionalClass;
		this.roadObstacles = roadObstacles;
		this.surfaceConditions = surfaceConditions;
		this.junction = junction;
		this.trafficControl = trafficControl;
		this.speedLimit = speedLimit;
		this.vehicleType = vehicleType;
		this.vehicleModelYear = vehicleModelYear;
		this.vehicleSpecialFunction = vehicleSpecialFunction;
		this.vehicleManoeuvre = vehicleManoeuvre;
		this.age = age;
		this.sex = sex;
		this.roadUserType = roadUserType;
		this.seatingPosition = seatingPosition;
		this.injurySeverity = injurySeverity;
		this.safetyEquipment = safetyEquipment;
		this.pedestrianManoeuvre = pedestrianManoeuvre;
		this.alcoholUse = alcoholUse;
		this.drugUse = drugUse;
		this.timeOfAdmission = timeOfAdmission;
		this.dayOfAdmission = dayOfAdmission;
		this.monthOfAdmission = monthOfAdmission;
		this.timeOfExit = timeOfExit;
		this.dayOfExit = dayOfExit;
		this.monthOfExit = monthOfExit;
		this.typeOfInjury = typeOfInjury;
	}
	public DayOfWeek getDayOfWeek() {
		return dayOfWeek;
	}
	public void setDayOfWeek(DayOfWeek dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public Set<Integer> getYear() {
		return year;
	}
	public void setYear(Set<Integer> year) {
		this.year = year;
	}
	public Set<LocalTime> getTimeOfCrash() {
		return timeOfCrash;
	}
	public void setTimeOfCrash(Set<LocalTime> timeOfCrash) {
		this.timeOfCrash = timeOfCrash;
	}
	public int getCrashType() {
		return crashType;
	}
	public void setCrashType(int crashType) {
		this.crashType = crashType;
	}
	public int getImpactType() {
		return impactType;
	}
	public void setImpactType(int impactType) {
		this.impactType = impactType;
	}
	public int getWeatherConditions() {
		return weatherConditions;
	}
	public void setWeatherConditions(int weatherConditions) {
		this.weatherConditions = weatherConditions;
	}
	public int getLightConditions() {
		return lightConditions;
	}
	public void setLightConditions(int lightConditions) {
		this.lightConditions = lightConditions;
	}
	public int getRoadwayType() {
		return roadwayType;
	}
	public void setRoadwayType(int roadwayType) {
		this.roadwayType = roadwayType;
	}
	public int getRoadFunctionalClass() {
		return roadFunctionalClass;
	}
	public void setRoadFunctionalClass(int roadFunctionalClass) {
		this.roadFunctionalClass = roadFunctionalClass;
	}
	public int getRoadObstacles() {
		return roadObstacles;
	}
	public void setRoadObstacles(int roadObstacles) {
		this.roadObstacles = roadObstacles;
	}
	public int getSurfaceConditions() {
		return surfaceConditions;
	}
	public void setSurfaceConditions(int surfaceConditions) {
		this.surfaceConditions = surfaceConditions;
	}
	public int getJunction() {
		return junction;
	}
	public void setJunction(int junction) {
		this.junction = junction;
	}
	public int getTrafficControl() {
		return trafficControl;
	}
	public void setTrafficControl(int trafficControl) {
		this.trafficControl = trafficControl;
	}
	public Set<Integer> getSpeedLimit() {
		return speedLimit;
	}
	public void setSpeedLimit(Set<Integer> speedLimit) {
		this.speedLimit = speedLimit;
	}
	public int getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(int vehicleType) {
		this.vehicleType = vehicleType;
	}
	public Set<Integer> getVehicleModelYear() {
		return vehicleModelYear;
	}
	public void setVehicleModelYear(Set<Integer> vehicleModelYear) {
		this.vehicleModelYear = vehicleModelYear;
	}
	public int getVehicleSpecialFunction() {
		return vehicleSpecialFunction;
	}
	public void setVehicleSpecialFunction(int vehicleSpecialFunction) {
		this.vehicleSpecialFunction = vehicleSpecialFunction;
	}
	public int getVehicleManoeuvre() {
		return vehicleManoeuvre;
	}
	public void setVehicleManoeuvre(int vehicleManoeuvre) {
		this.vehicleManoeuvre = vehicleManoeuvre;
	}
	public Set<Integer> getAge() {
		return age;
	}
	public void setAge(Set<Integer> age) {
		this.age = age;
	}
	public int getSex() {
		return sex;
	}
	public void setSex(int sex) {
		this.sex = sex;
	}
	public int getRoadUserType() {
		return roadUserType;
	}
	public void setRoadUserType(int roadUserType) {
		this.roadUserType = roadUserType;
	}
	public int getSeatingPosition() {
		return seatingPosition;
	}
	public void setSeatingPosition(int seatingPosition) {
		this.seatingPosition = seatingPosition;
	}
	public int getInjurySeverity() {
		return injurySeverity;
	}
	public void setInjurySeverity(int injurySeverity) {
		this.injurySeverity = injurySeverity;
	}
	public int getSafetyEquipment() {
		return safetyEquipment;
	}
	public void setSafetyEquipment(int safetyEquipment) {
		this.safetyEquipment = safetyEquipment;
	}
	public int getPedestrianManoeuvre() {
		return pedestrianManoeuvre;
	}
	public void setPedestrianManoeuvre(int pedestrianManoeuvre) {
		this.pedestrianManoeuvre = pedestrianManoeuvre;
	}
	public int getAlcoholUse() {
		return alcoholUse;
	}
	public void setAlcoholUse(int alcoholUse) {
		this.alcoholUse = alcoholUse;
	}
	public int getDrugUse() {
		return drugUse;
	}
	public void setDrugUse(int drugUse) {
		this.drugUse = drugUse;
	}
	public Set<LocalTime> getTimeOfAdmission() {
		return timeOfAdmission;
	}
	public void setTimeOfAdmission(Set<LocalTime> timeOfAdmission) {
		this.timeOfAdmission = timeOfAdmission;
	}
	public String getDayOfAdmission() {
		return dayOfAdmission;
	}
	public void setDayOfAdmission(String dayOfAdmission) {
		this.dayOfAdmission = dayOfAdmission;
	}
	public int getMonthOfAdmission() {
		return monthOfAdmission;
	}
	public void setMonthOfAdmission(int monthOfAdmission) {
		this.monthOfAdmission = monthOfAdmission;
	}
	public Set<LocalTime> getTimeOfExit() {
		return timeOfExit;
	}
	public void setTimeOfExit(Set<LocalTime> timeOfExit) {
		this.timeOfExit = timeOfExit;
	}
	public String getDayOfExit() {
		return dayOfExit;
	}
	public void setDayOfExit(String dayOfExit) {
		this.dayOfExit = dayOfExit;
	}
	public int getMonthOfExit() {
		return monthOfExit;
	}
	public void setMonthOfExit(int monthOfExit) {
		this.monthOfExit = monthOfExit;
	}
	public int getTypeOfInjury() {
		return typeOfInjury;
	}
	public void setTypeOfInjury(int typeOfInjury) {
		this.typeOfInjury = typeOfInjury;
	}
	public LocalDate getStart() {
		return start;
	}
	public void setStart(LocalDate start) {
		this.start = start;
	}
	public LocalDate getEnd() {
		return end;
	}
	public void setEnd(LocalDate end) {
		this.end = end;
	}
}
