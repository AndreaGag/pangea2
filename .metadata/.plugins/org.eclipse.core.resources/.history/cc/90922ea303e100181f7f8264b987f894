package it.floaty.adamsoser.web;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import it.floaty.adamsoser.domain.Crash;
import it.floaty.adamsoser.domain.Person;
import it.floaty.adamsoser.domain.Report;
import it.floaty.adamsoser.domain.Vehicle;
import it.floaty.adamsoser.persistence.ReportRepository;
import it.floaty.adamsoser.util.Constants;
import it.floaty.adamsoser.web.model.request.DateModel;
import it.floaty.adamsoser.web.model.request.FiltersModel;
import it.floaty.adamsoser.web.model.response.ResponseModel;

@RestController
public class GraphController {
	
	@Autowired
	private ReportRepository reportRepository;
	
	public ResponseModel getGraphData(DateModel dates) {
		Set<Report> reports = reportRepository.findByCrashDate(dates.getStart(), dates.getEnd());
		if(reports.size() > 0)
			return new ResponseModel(reports, 0, null);
		else
			return new ResponseModel(null, 1, "No results");
	}
	
	public ResponseModel getGraphDataFiltered(DateModel dates, FiltersModel model) {
		Set<Report> reports = reportRepository.findByCrashDate(dates.getStart(), dates.getEnd());
		LocalTime start = getFirstTimeFromZone(model.getTimeOfCrash());
		LocalTime end = getSecondTimeFromZone(model.getTimeOfCrash());
		reports = reports.stream()
				//Day of week	
				.filter(x -> this.filterByDayOfWeek(x.getCrash(), model.getDayOfWeek()))
										//x.getCrash().getDate().getDayOfWeek().equals(DayOfWeek.of(model.getDayOfWeek())))
				//Month	
				.filter(x -> this.filterByMonth(x.getCrash(), model.getMonth()))
										//x.getCrash().getDate().getMonth().equals(Month.of(model.getMonth())))
				//Time of crash	
				.filter(x -> x.getCrash().getTime().isAfter(start) && x.getCrash().getTime().isBefore(end))
				//Crash type
				.filter(x -> this.filterByCrashType(x.getCrash().getCrashType(), model.getCrashType()))
				//Impact type	
				.filter(x -> this.filterByImpactType(x.getCrash().getImpactType(), model.getImpactType()))
				//Weather conditions
				.filter(x -> x.getCrash().getWeatherConditions() == model.getWeatherConditions())
				//Light conditions	
				.filter(x -> x.getCrash().getLightConditions() == model.getLightConditions())
				//Roadway type
				.filter(x -> x.getRoad().getRoadwayType() == model.getRoadwayType())
				//Road functional class	
				.filter(x -> x.getRoad().getRoadFunctionalClass() == model.getRoadFunctionalClass())
				//Road obstacles
				.filter(x -> x.getRoad().isRoadObstacles() == model.getRoadObstacles())
				//Surface conditions
				.filter(x -> x.getRoad().getSurfaceConditions() == model.getSurfaceConditions())
				//Junction
				.filter(x -> x.getRoad().getJunction() == model.getJunction())
				//Traffic control
				.filter(x -> x.getRoad().getTrafficControl() == model.getTrafficControl())
				//Speed limit	
				.filter(x -> this.filterByRoadSpeedLimit(model.getSpeedLimit(), x.getRoad().getSpeedLimit()))
				//Vehicle type
				.filter(x -> this.filterByVehicleType(x.getVehicles(), model.getVehicleType()))
				//Vehicle model year
				.filter(x -> this.filterByVehicleModelYear(x.getVehicles(), model.getVehicleModelYear()))
				//Vehicle special function	
				.filter(x -> this.filterByVehicleSpecialFunction(x.getVehicles(), model.getVehicleSpecialFunction()))
				//Vehicle manoeuvre	
				.filter(x -> this.filterByVehicleManoeuvre(x.getVehicles(), model.getVehicleManoeuvre()))
				//Age
				.filter(x -> this.filterByPersonAge(x.getPersons(), model.getAge()))
				//Sex
				.filter(x -> this.filterByPersonSex(x.getPersons(), model.getSex()))
				//Road user type
				.filter(x -> this.filterByRoadUserType(x.getPersons(), model.getRoadUserType()))
				//Seating position
				.filter((x -> this.filterBySeatingPosition(x.getPersons(), model.getSeatingPosition())))
				//Injury severity
				.filter(x -> this.filterByInjurySeverity(x.getPersons(), model.getInjurySeverity()))
				//Safety equipment
				.filter(x -> this.filterBySafetyEquipment(x.getPersons(), model.getSafetyEquipment()))
				//Pedestrian manoeuvre
				.filter(x -> this.filterByPedestrianManouvre(x.getPersons(), model.getPedestrianManoeuvre()))
				//Alcohol use
				.filter(x -> this.filterByAlcoholUse(x.getPersons(), model.getAlcoholUse()))
				//Drug use
				.filter(x -> this.filterByDrugUse(x.getPersons(), model.getDrugUse()))
				
				//HOSPITAL
				//Time of admission
				//Day of admission	
				//Month of admission	
				//Time of exit	
				//Day of exit	
				//Month of exit	
				//Type of injury


				.filter(x -> x.getCrash().getDate().getDayOfWeek().equals(model.getDayOfWeek())).collect(Collectors.toSet());
			return null;
	}


//	Number of crashes
	 
//	Number of severe injured persons
	
//	Number of slightly injured persons
	
//	Number of deaths
	
//	Number of vehicles involved in crashes
	
//	Number of persons involved in crashes (including pedestrians)
	
//	Number of pedestrians involved in crashes

	private boolean filterByDayOfWeek(Crash crash, Set<Integer> dayOfWeek) {
		DayOfWeek ld = crash.getDate().getDayOfWeek();
		int dayWeek = ld.getValue();
		for(int dOW : dayOfWeek) {
			if(dOW == dayWeek) {
				return true;
			}
		}
		return false;
	}	

	private boolean filterByMonth(Crash crash, Set<Integer> month) {
		Month m = crash.getDate().getMonth();
		int monthCrash = m.getValue();
		for(int mo : month) {
			if(mo == monthCrash) {
				return true;
			}
		}
		return false;
	}
		
	private boolean filterByCrashType(int crashType, Set<Integer> crashTypeFilter) {
		for(int cTF : crashTypeFilter) {
			if(crashType == cTF) {
				return true;
			}
		}
		return false;
	}

	private boolean filterByImpactType(int impactType, Set<Integer> impactTypeFilter) {
		for(int iTF : impactTypeFilter) {
			if(iTF == impactType) {
				return true;
			}
		}
		return false;
	}
	private boolean filterByVehicleModelYear(Set<Vehicle> vehicles, Set<Integer> vehicleModelYear) {
		if(vehicleModelYear.contains(0)) {
			return true;
		}
		int currentYear = LocalDate.now().getYear();
		for(int yearsRangeFilter : vehicleModelYear) {
			for(Vehicle vehicle : vehicles) {
				if(yearsRangeFilter == 1 && currentYear - Integer.parseInt(vehicle.getVehicleModelYear())<=3) {
					return true;
				}
				if(yearsRangeFilter == 2 && currentYear - Integer.parseInt(vehicle.getVehicleModelYear())<=6) {
					return true;
				}
				if(yearsRangeFilter == 3 && currentYear - Integer.parseInt(vehicle.getVehicleModelYear())<=10) {
					return true;
				}
				if(yearsRangeFilter == 4 && currentYear - Integer.parseInt(vehicle.getVehicleModelYear())>10) {
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean filterByVehicleSpecialFunction(Set<Vehicle> vehicles, int vehicleSpecialFunction) {
		if(vehicleSpecialFunction == 0) {
			return true;
		}
		for(Vehicle vehicle : vehicles) {
			if(vehicle.getSpecialFunction() == vehicle.getSpecialFunction()) {
				return true;
			}
		}
		return false;
	}

	private boolean filterByVehicleType(Set<Vehicle> vehicles, int vehicleType) {
		for(Vehicle vehicle: vehicles) {
			if (vehicle.getVehicleType() == vehicleType)
				return true;
		}
		return false;
	}
	
	private boolean filterByRoadSpeedLimit(Set<Integer> speedLimitsFilter, int roadSpeedLimit) {
		if(speedLimitsFilter.contains(0)) {
			return true;
		}
		List<Integer> speedLimits = new ArrayList<>();
		for(int limitFilter : speedLimitsFilter) {
			
			if(limitFilter == 1) {
				speedLimits.addAll(Constants.FROM_ZERO_TO_THIRTY);
			}
			if(limitFilter == 2) {
				speedLimits.addAll(Constants.FROM_FORTY_TO_FIFTY);
			}
			if(limitFilter == 3) {
				speedLimits.addAll(Constants.FROM_SIXTY_TO_SEVENTY);
			}
			if(limitFilter == 4) {
				speedLimits.addAll(Constants.FROM_EIGHTY_TO_NINETY);
			}
			if(limitFilter == 5) {
				speedLimits.addAll(Constants.FROM_ONEHUNDERED_TO_ONEHUNDREDANDTEN);
			}
			if(limitFilter == 6) {
				speedLimits.addAll(Constants.FROM_ONEHUNDREDANDTWENTY_TO_ONEHUNDEREDANDTHIRTY);
			}
			if(limitFilter == 7) {
				speedLimits.addAll(Constants.OVER_ONEHUNDREDANDTHIRTY);
			}
		}
		for(int filter : speedLimits) {
			if(filter == roadSpeedLimit ) {
				return true;
			}
			if(filter == 140 && roadSpeedLimit >= 140) {
				return true;
			}
		}
		return false;
	}
		
	private boolean filterByVehicleManoeuvre(Set<Vehicle> vehicles, int vehicleManoeuvre) {
		if(vehicleManoeuvre == 0) {
			return true;
		}
		for(Vehicle vehicle : vehicles) {
			if(vehicle.getVehicleManoeuvre() == vehicleManoeuvre) {
				return true;
			}
		}
		return false;
	}

	private boolean filterByPersonAge(Set<Person> persons, Set<Integer> age) {
		LocalDate currentDate = LocalDate.now();
		
		if(age.contains(0)) {
			return true;
		}
		for(Person person : persons) {
			Period p = Period.between(person.getBirthDate(), currentDate);
			int personAge = p.getYears();
			if (personAge <= 14 && age.contains(1)) {
				return true;
			}
			if(personAge <= 17 && age.contains(2)) {
				return true;
			}
			if(personAge <= 24 && age.contains(3)) {
				return true;
			}
			if(personAge <= 49 && age.contains(4)) {
				return true;
			}
			if(personAge <= 64 && age.contains(5)) {
				return true;
			}
			if(personAge > 64 && age.contains(6)) {
				return true;
			}
		}
		return false;
	}

	private boolean filterByPersonSex(Set<Person> persons, int sex) {
		if(sex==0) {
			return true;
		}
		for(Person person : persons) {
			if(person.getSex() == sex) {
				return true;
			}
		}
		return false;
	}

	private boolean filterByRoadUserType(Set<Person> persons, int roadUserType) {
		if(roadUserType == 0) {
			return true;
		}
		for(Person person : persons) {
			if(person.getRoadUserType() == roadUserType) {
				return true;
			}
		}
		return false;
	}
	
	private boolean filterBySeatingPosition(Set<Person> persons, int seatingPosition) {
		if(seatingPosition == 0) {
			return true;
		}
		for(Person person : persons) {
			if(person.getSeatingPosition() == seatingPosition) {
				return true;
			}
		}
		return false;
	}

	private boolean filterByInjurySeverity(Set<Person> persons, int injurySeverity) {
		if(injurySeverity == 0) {
			return true;
		}
		for(Person person : persons) {
			if(person.getInjuryServerity() == injurySeverity) {
				return true;
			}
		}
		return false;
	}
	
	private boolean filterBySafetyEquipment(Set<Person> persons, int safetyEquipment) {
		if(safetyEquipment == 0) {
			return true;
		}
		for(Person person : persons) {
			if(person.getSafetyEquipment() == safetyEquipment) {
				return true;
			}
		}
		return false;
	}

	private boolean filterByPedestrianManouvre(Set<Person> persons, int pedestrianManoeuvre) {
		if(pedestrianManoeuvre == 0) {
			return true;
		}
		for(Person person : persons) {
			if(person.getPedestrianManoeuvre() == pedestrianManoeuvre) {
				return true;
			}
		}
		return false;
	}
		
	private boolean filterByAlcoholUse(Set<Person> persons, int alcoholUse) {
		if(alcoholUse==0) {
			return true;
		}
		for(Person person : persons) {
			if(person.getAlcoholUse() == alcoholUse) {
				return true;
			}
		}
		return false;
	}
		
	private boolean filterByDrugUse(Set<Person> persons, int drugUse) {
		if(drugUse == 0) {
			return true;
		}
		for(Person person : persons) {
			if(person.getDrugUse() == drugUse) {
				return true;
			}
		}
		return false;
	}
	
	private LocalTime getFirstTimeFromZone(int zone) {
		switch (zone) {
		case 0:
			return Constants.TIME_ZERO_FIRST;
		case 1:
			return Constants.TIME_FIRST_FIRST;
		case 2:
			return Constants.TIME_SECOND_FIRST;
		case 3:
			return Constants.TIME_THIRD_FIRST;
		case 4:
			return Constants.TIME_FOURTH_FIRST;
		case 5:
			return Constants.TIME_FIFTH_FIRST;
		case 6:
			return Constants.TIME_SIXTH_FIRST;
		default:
			return Constants.TIME_ZERO_FIRST;
		}
	}
	
	private LocalTime getSecondTimeFromZone(int zone) {
		switch (zone) {
		case 0:
			return Constants.TIME_ZERO_SECOND;
		case 1:
			return Constants.TIME_FIRST_SECOND;
		case 2:
			return Constants.TIME_SECOND_SECOND;
		case 3:
			return Constants.TIME_THIRD_SECOND;
		case 4:
			return Constants.TIME_FOURTH_SECOND;
		case 5:
			return Constants.TIME_FIFTH_SECOND;
		case 6:
			return Constants.TIME_SIXTH_SECOND;
		default:
			return Constants.TIME_ZERO_SECOND;
		}
	}
}
