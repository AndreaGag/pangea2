package it.floaty.adams.domain;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Person {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@NotNull
	private String personId;
	@NotNull
	private String name;
	@NotNull
	private int sex;
	private LocalDate birthDate;

	//POLICE
	private String occupantVehicleNumber;
	private String pedestrianVehicleNumber;
	private int roadUserType;
	private int seatingPosition;
	private int injurySeverity;
	private int safetyEquipment;
	private int pedestrianManoeuvre;
	private int alcoholUse;
	private int drugUse;
	
	//HOSPITAL
	private LocalDate dateExit;
	private LocalTime timeExit;
	private int typeOfInjury;
	private int finalDiagnosis;
	
	public Person(long id, String personId, String name, int sex, LocalDate birthDate, String occupantVehicleNumber,
			String pedestrianVehicleNumber, int roadUserType, int seatingPosition, int injurySeverity,
			int safetyEquipment, int pedestrianManoeuvre, int alcoholUse, int drugUse) {
		super();
		this.id = id;
		this.personId = personId;
		this.name = name;
		this.sex = sex;
		this.birthDate = birthDate;
		this.occupantVehicleNumber = occupantVehicleNumber;
		this.pedestrianVehicleNumber = pedestrianVehicleNumber;
		this.roadUserType = roadUserType;
		this.seatingPosition = seatingPosition;
		this.injurySeverity = injurySeverity;
		this.safetyEquipment = safetyEquipment;
		this.pedestrianManoeuvre = pedestrianManoeuvre;
		this.alcoholUse = alcoholUse;
		this.drugUse = drugUse;
	}
	
	public Person() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Person(long id, String personId, String name, int sex, LocalDate birthDate, LocalDate dateExit,
			LocalTime timeExit, int typeOfInjury, int finalDiagnosis) {
		super();
		this.id = id;
		this.personId = personId;
		this.name = name;
		this.sex = sex;
		this.birthDate = birthDate;
		this.dateExit = dateExit;
		this.timeExit = timeExit;
		this.typeOfInjury = typeOfInjury;
		this.finalDiagnosis = finalDiagnosis;
	}

	public long getId() {
		return id;
	}
	public String getPersonId() {
		return personId;
	}
	public void setPersonId(String personId) {
		this.personId = personId;
	}
	public String getOccupantVehicleNumber() {
		return occupantVehicleNumber;
	}
	public void setOccupantVehicleNumber(String occupantVehicleNumber) {
		this.occupantVehicleNumber = occupantVehicleNumber;
	}
	public String getPedestrianVehicleNumber() {
		return pedestrianVehicleNumber;
	}
	public void setPedestrianVehicleNumber(String pedestrianVehicleNumber) {
		this.pedestrianVehicleNumber = pedestrianVehicleNumber;
	}
	public LocalDate getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}
	public int getSex() {
		return sex;
	}
	public void setSex(int sex) {
		this.sex = sex;
	}
	public int getRoadUserType() {
		return roadUserType;
	}
	public void setRoadUserType(int roadUserType) {
		this.roadUserType = roadUserType;
	}
	public int getSeatingPosition() {
		return seatingPosition;
	}
	public void setSeatingPosition(int seatingPosition) {
		this.seatingPosition = seatingPosition;
	}
	public int getInjuryServerity() {
		return injurySeverity;
	}
	public void setInjuryServerity(int injuryServerity) {
		this.injurySeverity = injuryServerity;
	}
	public int getSafetyEquipment() {
		return safetyEquipment;
	}
	public void setSafetyEquipment(int safetyEquipment) {
		this.safetyEquipment = safetyEquipment;
	}
	public int getPedestrianManoeuvre() {
		return pedestrianManoeuvre;
	}
	public void setPedestrianManoeuvre(int pedestrianManoeuvre) {
		this.pedestrianManoeuvre = pedestrianManoeuvre;
	}
	public int getAlcoholUse() {
		return alcoholUse;
	}
	public void setAlcoholUse(int alcoholUse) {
		this.alcoholUse = alcoholUse;
	}
	public int getDrugUse() {
		return drugUse;
	}
	public void setDrugUse(int drugUse) {
		this.drugUse = drugUse;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public LocalDate getDateExit() {
		return dateExit;
	}
	public void setDateExit(LocalDate dateExit) {
		this.dateExit = dateExit;
	}
	public LocalTime getTimeExit() {
		return timeExit;
	}
	public void setTimeExit(LocalTime timeExit) {
		this.timeExit = timeExit;
	}
	public int getTypeOfInjury() {
		return typeOfInjury;
	}
	public void setTypeOfInjury(int typeOfInjury) {
		this.typeOfInjury = typeOfInjury;
	}
	public int getFinalDiagnosis() {
		return finalDiagnosis;
	}
	public void setFinalDiagnosis(int finalDiagnosis) {
		this.finalDiagnosis = finalDiagnosis;
	}
}
