package it.floaty.adamsoser.web.model.request;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;

public class DateModel {
	
	@NotNull
	private LocalDate start;
	@NotNull
	private LocalDate end;
	
	public DateModel() {
		super();
	}

	public DateModel(@NotNull LocalDate start, @NotNull LocalDate end) {
		super();
		this.start = start;
		this.end = end;
	}
	
	public LocalDate getStart() {
		return start;
	}
	public void setStart(LocalDate start) {
		this.start = start;
	}
	public LocalDate getEnd() {
		return end;
	}
	public void setEnd(LocalDate end) {
		this.end = end;
	}
}
