package it.floaty.adamsoser.web.model.request;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Set;

public class FiltersModel {

	private Set<Integer> dayOfWeek;
	private Set<Integer> month;
	private Set<Integer> year;
	private int timeOfCrash;
	private Set<Integer> crashType;
	private Set<Integer> impactType;
	private Set<Integer> weatherConditions;
	private Set<Integer> lightConditions;
	private Set<Integer> roadwayType;
	private Set<Integer> roadFunctionalClass;
	private Set<Integer> roadObstacles;
	private Set<Integer> surfaceConditions;
	private Set<Integer> junction;
	private Set<Integer> trafficControl;
	private Set<Integer> speedLimit;
	private Set<Integer> vehicleType;
	private Set<Integer> vehicleModelYear;
	private Set<Integer> vehicleSpecialFunction;
	private Set<Integer> vehicleManoeuvre;
	private Set<Integer> age;
	private Set<Integer> sex;
	private Set<Integer> roadUserType;
	private Set<Integer> seatingPosition;
	private Set<Integer> injurySeverity;
	private Set<Integer> safetyEquipment;
	private Set<Integer> pedestrianManoeuvre;
	private Set<Integer> alcoholUse;
	private Set<Integer> drugUse;
	private Set<LocalTime> timeOfAdmission;
	private Set<Integer> dayOfAdmission;
	private Set<Integer> monthOfAdmission;
	private Set<LocalTime> timeOfExit;
	private Set<Integer> dayOfExit;
	private Set<Integer> monthOfExit;
	private Set<Integer> typeOfInjury;
	
	public FiltersModel() {
		super();
	}
	
	public FiltersModel(Set<Integer> dayOfWeek, Set<Integer> month,
			Set<Integer> year, int timeOfCrash, Set<Integer> crashType, Set<Integer> impactType, Set<Integer> weatherConditions,
			Set<Integer> lightConditions, Set<Integer> roadwayType, Set<Integer> roadFunctionalClass, Set<Integer> roadObstacles, Set<Integer> surfaceConditions,
			Set<Integer> junction, Set<Integer> trafficControl, Set<Integer> speedLimit, Set<Integer> vehicleType, Set<Integer> vehicleModelYear,
			Set<Integer> vehicleSpecialFunction, Set<Integer> vehicleManoeuvre, Set<Integer> age, Set<Integer> sex, Set<Integer> roadUserType,
			Set<Integer> seatingPosition, Set<Integer> injurySeverity, Set<Integer> safetyEquipment, Set<Integer> pedestrianManoeuvre, Set<Integer> alcoholUse,
			Set<Integer> drugUse, Set<LocalTime> timeOfAdmission, Set<Integer> dayOfAdmission, Set<Integer> monthOfAdmission,
			Set<LocalTime> timeOfExit, Set<Integer> dayOfExit, Set<Integer> monthOfExit, Set<Integer> typeOfInjury) {
		super();
		this.dayOfWeek = dayOfWeek;
		this.month = month;
		this.year = year;
		this.timeOfCrash = timeOfCrash;
		this.crashType = crashType;
		this.impactType = impactType;
		this.weatherConditions = weatherConditions;
		this.lightConditions = lightConditions;
		this.roadwayType = roadwayType;
		this.roadFunctionalClass = roadFunctionalClass;
		this.roadObstacles = roadObstacles;
		this.surfaceConditions = surfaceConditions;
		this.junction = junction;
		this.trafficControl = trafficControl;
		this.speedLimit = speedLimit;
		this.vehicleType = vehicleType;
		this.vehicleModelYear = vehicleModelYear;
		this.vehicleSpecialFunction = vehicleSpecialFunction;
		this.vehicleManoeuvre = vehicleManoeuvre;
		this.age = age;
		this.sex = sex;
		this.roadUserType = roadUserType;
		this.seatingPosition = seatingPosition;
		this.injurySeverity = injurySeverity;
		this.safetyEquipment = safetyEquipment;
		this.pedestrianManoeuvre = pedestrianManoeuvre;
		this.alcoholUse = alcoholUse;
		this.drugUse = drugUse;
		this.timeOfAdmission = timeOfAdmission;
		this.dayOfAdmission = dayOfAdmission;
		this.monthOfAdmission = monthOfAdmission;
		this.timeOfExit = timeOfExit;
		this.dayOfExit = dayOfExit;
		this.monthOfExit = monthOfExit;
		this.typeOfInjury = typeOfInjury;
	}

	public Set<Integer> getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(Set<Integer> dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public Set<Integer> getMonth() {
		return month;
	}

	public void setMonth(Set<Integer> month) {
		this.month = month;
	}

	public Set<Integer> getYear() {
		return year;
	}

	public void setYear(Set<Integer> year) {
		this.year = year;
	}

	public int getTimeOfCrash() {
		return timeOfCrash;
	}

	public void setTimeOfCrash(int timeOfCrash) {
		this.timeOfCrash = timeOfCrash;
	}

	public Set<Integer> getCrashType() {
		return crashType;
	}

	public void setCrashType(Set<Integer> crashType) {
		this.crashType = crashType;
	}

	public Set<Integer> getImpactType() {
		return impactType;
	}

	public void setImpactType(Set<Integer> impactType) {
		this.impactType = impactType;
	}

	public Set<Integer> getWeatherConditions() {
		return weatherConditions;
	}

	public void setWeatherConditions(Set<Integer> weatherConditions) {
		this.weatherConditions = weatherConditions;
	}

	public Set<Integer> getLightConditions() {
		return lightConditions;
	}

	public void setLightConditions(Set<Integer> lightConditions) {
		this.lightConditions = lightConditions;
	}

	public Set<Integer> getRoadwayType() {
		return roadwayType;
	}

	public void setRoadwayType(Set<Integer> roadwayType) {
		this.roadwayType = roadwayType;
	}

	public Set<Integer> getRoadFunctionalClass() {
		return roadFunctionalClass;
	}

	public void setRoadFunctionalClass(Set<Integer> roadFunctionalClass) {
		this.roadFunctionalClass = roadFunctionalClass;
	}

	public Set<Integer> getRoadObstacles() {
		return roadObstacles;
	}

	public void setRoadObstacles(Set<Integer> roadObstacles) {
		this.roadObstacles = roadObstacles;
	}

	public Set<Integer> getSurfaceConditions() {
		return surfaceConditions;
	}

	public void setSurfaceConditions(Set<Integer> surfaceConditions) {
		this.surfaceConditions = surfaceConditions;
	}

	public Set<Integer> getJunction() {
		return junction;
	}

	public void setJunction(Set<Integer> junction) {
		this.junction = junction;
	}

	public Set<Integer> getTrafficControl() {
		return trafficControl;
	}

	public void setTrafficControl(Set<Integer> trafficControl) {
		this.trafficControl = trafficControl;
	}

	public Set<Integer> getSpeedLimit() {
		return speedLimit;
	}

	public void setSpeedLimit(Set<Integer> speedLimit) {
		this.speedLimit = speedLimit;
	}

	public Set<Integer> getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(Set<Integer> vehicleType) {
		this.vehicleType = vehicleType;
	}

	public Set<Integer> getVehicleModelYear() {
		return vehicleModelYear;
	}

	public void setVehicleModelYear(Set<Integer> vehicleModelYear) {
		this.vehicleModelYear = vehicleModelYear;
	}

	public Set<Integer> getVehicleSpecialFunction() {
		return vehicleSpecialFunction;
	}

	public void setVehicleSpecialFunction(Set<Integer> vehicleSpecialFunction) {
		this.vehicleSpecialFunction = vehicleSpecialFunction;
	}

	public Set<Integer> getVehicleManoeuvre() {
		return vehicleManoeuvre;
	}

	public void setVehicleManoeuvre(Set<Integer> vehicleManoeuvre) {
		this.vehicleManoeuvre = vehicleManoeuvre;
	}

	public Set<Integer> getAge() {
		return age;
	}

	public void setAge(Set<Integer> age) {
		this.age = age;
	}

	public Set<Integer> getSex() {
		return sex;
	}

	public void setSex(Set<Integer> sex) {
		this.sex = sex;
	}

	public Set<Integer> getRoadUserType() {
		return roadUserType;
	}

	public void setRoadUserType(Set<Integer> roadUserType) {
		this.roadUserType = roadUserType;
	}

	public Set<Integer> getSeatingPosition() {
		return seatingPosition;
	}

	public void setSeatingPosition(Set<Integer> seatingPosition) {
		this.seatingPosition = seatingPosition;
	}

	public Set<Integer> getInjurySeverity() {
		return injurySeverity;
	}

	public void setInjurySeverity(Set<Integer> injurySeverity) {
		this.injurySeverity = injurySeverity;
	}

	public Set<Integer> getSafetyEquipment() {
		return safetyEquipment;
	}

	public void setSafetyEquipment(Set<Integer> safetyEquipment) {
		this.safetyEquipment = safetyEquipment;
	}

	public Set<Integer> getPedestrianManoeuvre() {
		return pedestrianManoeuvre;
	}

	public void setPedestrianManoeuvre(Set<Integer> pedestrianManoeuvre) {
		this.pedestrianManoeuvre = pedestrianManoeuvre;
	}

	public Set<Integer> getAlcoholUse() {
		return alcoholUse;
	}

	public void setAlcoholUse(Set<Integer> alcoholUse) {
		this.alcoholUse = alcoholUse;
	}

	public Set<Integer> getDrugUse() {
		return drugUse;
	}

	public void setDrugUse(Set<Integer> drugUse) {
		this.drugUse = drugUse;
	}

	public Set<LocalTime> getTimeOfAdmission() {
		return timeOfAdmission;
	}

	public void setTimeOfAdmission(Set<LocalTime> timeOfAdmission) {
		this.timeOfAdmission = timeOfAdmission;
	}

	public Set<Integer> getDayOfAdmission() {
		return dayOfAdmission;
	}

	public void setDayOfAdmission(Set<Integer> dayOfAdmission) {
		this.dayOfAdmission = dayOfAdmission;
	}

	public Set<Integer> getMonthOfAdmission() {
		return monthOfAdmission;
	}

	public void setMonthOfAdmission(Set<Integer> monthOfAdmission) {
		this.monthOfAdmission = monthOfAdmission;
	}

	public Set<LocalTime> getTimeOfExit() {
		return timeOfExit;
	}

	public void setTimeOfExit(Set<LocalTime> timeOfExit) {
		this.timeOfExit = timeOfExit;
	}

	public Set<Integer> getDayOfExit() {
		return dayOfExit;
	}

	public void setDayOfExit(Set<Integer> dayOfExit) {
		this.dayOfExit = dayOfExit;
	}

	public Set<Integer> getMonthOfExit() {
		return monthOfExit;
	}

	public void setMonthOfExit(Set<Integer> monthOfExit) {
		this.monthOfExit = monthOfExit;
	}

	public Set<Integer> getTypeOfInjury() {
		return typeOfInjury;
	}

	public void setTypeOfInjury(Set<Integer> typeOfInjury) {
		this.typeOfInjury = typeOfInjury;
	}
	
}
