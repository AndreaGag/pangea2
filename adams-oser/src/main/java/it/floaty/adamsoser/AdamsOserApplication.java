package it.floaty.adamsoser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages= {"it.floaty.adamsoser"})
@EntityScan(basePackages = {"it.floaty.adamsoser"})
@ComponentScan(basePackages = { "it.floaty.adamsoser"})
public class AdamsOserApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdamsOserApplication.class, args);
	}
}
