package it.floaty.adamsoser.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Base {
	
	@Id
	private long id;
	
	@NotNull
	@Column(unique = true)
	private String name;
	
	@NotNull
	private int type;
	
	public Base() {
		super();
	}

	public Base(@NotNull String name, int type) {
		super();
		this.name = name;
		this.type = type;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getId() {
		return id;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
}
