package it.floaty.adamsoser.domain;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
public class Report {

	@Id
	private long id;

	private boolean valid;
	
	@OneToOne(fetch=FetchType.EAGER)
	@Cascade(CascadeType.PERSIST)
	private Road road;
	
	@OneToOne(fetch=FetchType.EAGER)
	@Cascade(CascadeType.PERSIST)
	private Crash crash;
	
	@OneToMany(fetch=FetchType.EAGER)
	@Cascade(CascadeType.PERSIST)
	private Set<Person> persons;
	
	@OneToMany(fetch=FetchType.EAGER)
	@Cascade(CascadeType.PERSIST)
	private Set<Vehicle> vehicles;
	
	public Report(boolean valid, Road road, Crash crash, Set<Person> persons, Set<Vehicle> vehicles) {
		super();
		this.valid = valid;
		this.road = road;
		this.crash = crash;
		this.persons = persons;
		this.vehicles = vehicles;
	}
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	public Road getRoad() {
		return road;
	}
	public void setRoad(Road road) {
		this.road = road;
	}
	public Crash getCrash() {
		return crash;
	}
	public void setCrash(Crash crash) {
		this.crash = crash;
	}
	public Set<Person> getPersons() {
		return persons;
	}
	public void setPersons(Set<Person> persons) {
		this.persons = persons;
	}
	public Set<Vehicle> getVehicles() {
		return vehicles;
	}
	public void setVehicles(Set<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}
	public long getId() {
		return id;
	}
}
