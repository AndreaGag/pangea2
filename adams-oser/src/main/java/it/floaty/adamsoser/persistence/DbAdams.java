package it.floaty.adamsoser.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DbAdams implements CommandLineRunner{

	@Autowired
	private CrashRepository crashRepository;
	@Autowired
	private PersonRepository personRepository;
	@Autowired
	private RoadRepository roadRepository;
	@Autowired
	private VehicleRepository vehicleRepository;
	@Autowired
	private BaseRepository baseRepository;
	@Autowired
	private ReportRepository reportRepository;
	
	public DbAdams(CrashRepository crashRepository, PersonRepository personRepository, RoadRepository roadRepository,
			VehicleRepository vehicleRepository, BaseRepository baseRepository,
			ReportRepository reportRepository) {
		super();
		this.crashRepository = crashRepository;
		this.personRepository = personRepository;
		this.roadRepository = roadRepository;
		this.vehicleRepository = vehicleRepository;
		this.baseRepository = baseRepository;
		this.reportRepository = reportRepository;
	}



	@Override
	public void run(String... args) throws Exception {
	}

}
