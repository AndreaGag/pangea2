package it.floaty.adamsoser.persistence;

import java.time.LocalDate;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import it.floaty.adamsoser.domain.Report;

@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {

	public Set<Report> findByValid(boolean valid);
	
	@Query("Select r from Report r where r.Crash.date >= ?1 and r.Crash.date <= ?2 and r.valid == true")
	public Set<Report> findByCrashDate(LocalDate start, LocalDate end);
	
	@Query("Select r from Report r where r.Crash.time >= ?1 and r.Crash.time <= ?2 and r.valid == true")
	public Set<Report> findByCrashTime(LocalDate start, LocalDate end);
	
	
}
