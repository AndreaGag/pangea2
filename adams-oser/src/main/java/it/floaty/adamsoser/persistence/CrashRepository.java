package it.floaty.adamsoser.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.floaty.adamsoser.domain.Crash;

@Repository
public interface CrashRepository extends JpaRepository<Crash, Long>{

	public Crash findFirstByOrderByIdDesc();
}
