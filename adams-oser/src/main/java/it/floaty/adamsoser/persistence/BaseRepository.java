package it.floaty.adamsoser.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.floaty.adamsoser.domain.Base;

@Repository
public interface BaseRepository extends JpaRepository<Base, Long> {

}
