package it.floaty.adamsoser.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.floaty.adamsoser.domain.Road;

@Repository
public interface RoadRepository extends JpaRepository<Road, Long> {

}
