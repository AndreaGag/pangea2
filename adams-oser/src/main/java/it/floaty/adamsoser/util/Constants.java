package it.floaty.adamsoser.util;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;


public class Constants {

	public final static String ADAMS_KEY = "NDFNDS46-ADSFNC323_RESO7";
	
	public final static LocalTime  TIME_ZERO_FIRST = LocalTime.of(6, 0);
	public final static LocalTime  TIME_ZERO_SECOND = LocalTime.of(6, 0);
	public final static LocalTime  TIME_FIRST_FIRST = LocalTime.of(6, 0);
	public final static LocalTime  TIME_FIRST_SECOND = LocalTime.of(9, 59);
	public final static LocalTime  TIME_SECOND_FIRST = LocalTime.of(10, 0);
	public final static LocalTime  TIME_SECOND_SECOND = LocalTime.of(12, 59);
	public final static LocalTime  TIME_THIRD_FIRST = LocalTime.of(13, 0);
	public final static LocalTime  TIME_THIRD_SECOND = LocalTime.of(15, 59);
	public final static LocalTime  TIME_FOURTH_FIRST = LocalTime.of(16, 0);
	public final static LocalTime  TIME_FOURTH_SECOND = LocalTime.of(18, 59);
	public final static LocalTime  TIME_FIFTH_FIRST = LocalTime.of(19, 0);
	public final static LocalTime  TIME_FIFTH_SECOND = LocalTime.of(21, 59);
	public final static LocalTime  TIME_SIXTH_FIRST = LocalTime.of(22, 0);
	public final static LocalTime  TIME_SIXTH_SECOND = LocalTime.of(5, 59);
	 
	public final static List<Integer> FROM_ZERO_TO_THIRTY = new ArrayList<>(zeroThirty());
	public final static List<Integer> FROM_FORTY_TO_FIFTY = new ArrayList<>(fortyFifty());
	public final static List<Integer> FROM_SIXTY_TO_SEVENTY = new ArrayList<>(sixtySeventy());
	public final static List<Integer> FROM_EIGHTY_TO_NINETY = new ArrayList<>(eightyNinety());
	public final static List<Integer> FROM_ONEHUNDERED_TO_ONEHUNDREDANDTEN = new ArrayList<>(oneHoundredOneHoundredAndTen());
	public final static List<Integer> FROM_ONEHUNDREDANDTWENTY_TO_ONEHUNDEREDANDTHIRTY = new ArrayList<>(oneHoundredAndTwentyOneHoundredAndThirty());
	public final static List<Integer> OVER_ONEHUNDREDANDTHIRTY = new ArrayList<>(overOneHoundredAndThirty());
	
	private static List<Integer> zeroThirty() {
		List<Integer> zeroThirty = new ArrayList<>();
		zeroThirty.add(0);
		zeroThirty.add(10);
		zeroThirty.add(20);
		zeroThirty.add(30);
		return zeroThirty;
	}
	
	private static List<Integer> fortyFifty() {
		List<Integer> fortyFifty = new ArrayList<>();
		fortyFifty.add(40);
		fortyFifty.add(50);
		return fortyFifty;
	}
	
	private static List<Integer> sixtySeventy() {
		List<Integer> sixtySeventy = new ArrayList<>();
		sixtySeventy.add(60);
		sixtySeventy.add(70);
		return sixtySeventy;
	}
	
	private static List<Integer> eightyNinety() {
		List<Integer> eightyNinety = new ArrayList<>();
		eightyNinety.add(80);
		eightyNinety.add(90);
		return eightyNinety;
	}
	
	private static List<Integer> oneHoundredOneHoundredAndTen() {
		List<Integer> oneHoundredOneHoundredAndTen = new ArrayList<>();
		oneHoundredOneHoundredAndTen.add(100);
		oneHoundredOneHoundredAndTen.add(110);
		return oneHoundredOneHoundredAndTen;
	}
	
	private static List<Integer> oneHoundredAndTwentyOneHoundredAndThirty() {
		List<Integer> oneHoundredAndTwentyOneHoundredAndThirty = new ArrayList<>();
		oneHoundredAndTwentyOneHoundredAndThirty.add(120);
		oneHoundredAndTwentyOneHoundredAndThirty.add(130);
		return oneHoundredAndTwentyOneHoundredAndThirty;
	}
	
	private static List<Integer> overOneHoundredAndThirty() {
		List<Integer> overOneHoundredAndThirty = new ArrayList<>();
		overOneHoundredAndThirty.add(140);
		return overOneHoundredAndThirty;
	}
}
